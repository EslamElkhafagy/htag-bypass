package com.byba.health.backEnd.entities;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "sessions")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Sessions.findAll", query = "SELECT u FROM Sessions u"),
        @NamedQuery(name = "Sessions.findByUserID", query = "SELECT u FROM Sessions u WHERE u.userProfile.userId=:userId"),
        @NamedQuery(name = "Sessions.findBySession", query = "SELECT u FROM Sessions u WHERE u.Session=:session")})

public class Sessions implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Integer ID;
    @Column(name = "Session")
    private String Session;
    @Column(name = "Status")
    private String Status;

    @JoinColumn(name = "User_ID", referencedColumnName = "user_id")
    @ManyToOne (optional = false)
    private UserProfile userProfile;


    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getSession() {
        return Session;
    }

    public void setSession(String session) {
        Session = session;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }


    @Override
    public String toString() {
        return "Sessions{" +
                "ID=" + ID +
                ", Session='" + Session + '\'' +
                ", Status='" + Status + '\'' +
                ", userProfile=" + userProfile +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Sessions sessions = (Sessions) o;
        return Objects.equals(ID, sessions.ID) &&
                Objects.equals(Session, sessions.Session) &&
                Objects.equals(Status, sessions.Status) &&
                Objects.equals(userProfile, sessions.userProfile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ID, Session, Status, userProfile);
    }
}
