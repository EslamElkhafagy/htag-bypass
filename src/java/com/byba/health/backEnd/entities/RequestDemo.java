package com.byba.health.backEnd.entities;

/**
 * Created by Administrator on 12/6/2018.
 */

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "request_demo")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "RequestDemo.findAll", query = "SELECT r FROM RequestDemo r")
        , @NamedQuery(name = "RequestDemo.findByMobile", query = "SELECT r FROM RequestDemo r WHERE r.mobile = :mobile")
        , @NamedQuery(name = "RequestDemo.findByAddress", query = "SELECT r FROM RequestDemo r WHERE r.address = :address")
        , @NamedQuery(name = "RequestDemo.findByEmail", query = "SELECT r FROM RequestDemo r WHERE r.email = :email")
        , @NamedQuery(name = "RequestDemo.findByName", query = "SELECT r FROM RequestDemo r WHERE r.name = :name")
        , @NamedQuery(name = "RequestDemo.findByType", query = "SELECT r FROM RequestDemo r WHERE r.type = :type")
        , @NamedQuery(name = "RequestDemo.findByNotes", query = "SELECT r FROM RequestDemo r WHERE r.notes = :notes")
        , @NamedQuery(name = "RequestDemo.findById", query = "SELECT r FROM RequestDemo r WHERE r.id = :id")})
public class RequestDemo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 200)
    @Column(name = "mobile")
    private String mobile;
    @Size(max = 1000)
    @Column(name = "address")
    private String address;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 200)
    @Column(name = "email")
    private String email;
    @Size(max = 200)
    @Column(name = "Name")
    private String name;
    @Size(max = 200)
    @Column(name = "type")
    private String type;
    @Size(max = 2000)
    @Column(name = "notes")
    private String notes;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;

    public RequestDemo() {
    }

    public RequestDemo(Long id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RequestDemo)) {
            return false;
        }
        RequestDemo other = (RequestDemo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.RequestDemo[ id=" + id + " ]";
    }

}
