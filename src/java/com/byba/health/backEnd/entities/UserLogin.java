/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;


import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "user_login")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "UserLogin.findAll", query = "SELECT u FROM UserLogin u"),
        @NamedQuery(name = "UserLogin.findByUserProfileUserId", query = "SELECT u FROM UserLogin u WHERE u.userProfileUserId = :userProfileUserId"),
        @NamedQuery(name = "UserLogin.findByActivateDate", query = "SELECT u FROM UserLogin u WHERE u.activateDate = :activateDate"),
        @NamedQuery(name = "UserLogin.findByUserStatus", query = "SELECT u FROM UserLogin u WHERE u.userStatus = :userStatus")})
public class UserLogin implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_profile_user_id")
    private Long userProfileUserId;
    @Column(name = "activate_date")
    @Temporal(TemporalType.DATE)
    private Date activateDate;
    @Column(name = "user_status")
    private String userStatus;
    @JoinColumn(name = "user_profile_user_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public UserLogin() {
    }

    public UserLogin(Long userProfileUserId) {
        this.userProfileUserId = userProfileUserId;
    }

    public Long getUserProfileUserId() {
        return userProfileUserId;
    }

    public void setUserProfileUserId(Long userProfileUserId) {
        this.userProfileUserId = userProfileUserId;
    }

    public Date getActivateDate() {
        return activateDate;
    }

    public void setActivateDate(Date activateDate) {
        this.activateDate = activateDate;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userProfileUserId != null ? userProfileUserId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserLogin)) {
            return false;
        }
        UserLogin other = (UserLogin) object;
        if ((this.userProfileUserId == null && other.userProfileUserId != null) || (this.userProfileUserId != null && !this.userProfileUserId.equals(other.userProfileUserId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.UserLogin[ userProfileUserId=" + userProfileUserId + " ]";
    }

}
