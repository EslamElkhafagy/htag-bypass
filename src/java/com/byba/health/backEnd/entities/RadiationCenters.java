/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "radiation_centers")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RadiationCenters.findAll", query = "SELECT r FROM RadiationCenters r"),
    @NamedQuery(name = "RadiationCenters.findByRadId", query = "SELECT r FROM RadiationCenters r WHERE r.radId = :radId"),
    @NamedQuery(name = "RadiationCenters.findByRadName", query = "SELECT r FROM RadiationCenters r WHERE r.radName = :radName"),
    @NamedQuery(name = "RadiationCenters.findByEmail", query = "SELECT r FROM RadiationCenters r WHERE UPPER(r.email) LIKE UPPER(:email)"),
    @NamedQuery(name = "RadiationCenters.findByPassword", query = "SELECT r FROM RadiationCenters r WHERE r.password = :password"),
    @NamedQuery(name = "RadiationCenters.findByMobileNo", query = "SELECT r FROM RadiationCenters r WHERE r.mobileNo = :mobileNo"),
    @NamedQuery(name = "RadiationCenters.findByAddMobileNo", query = "SELECT r FROM RadiationCenters r WHERE r.addMobileNo = :addMobileNo"),
    @NamedQuery(name = "RadiationCenters.findByGovernment", query = "SELECT r FROM RadiationCenters r WHERE r.government = :government"),
    @NamedQuery(name = "RadiationCenters.findByArea", query = "SELECT r FROM RadiationCenters r WHERE r.area = :area"),
    @NamedQuery(name = "RadiationCenters.findByAddress", query = "SELECT r FROM RadiationCenters r WHERE r.address = :address"),
    @NamedQuery(name = "RadiationCenters.findByNationalId", query = "SELECT r FROM RadiationCenters r WHERE r.nationalId = :nationalId"),
    @NamedQuery(name = "RadiationCenters.findByLicense", query = "SELECT r FROM RadiationCenters r WHERE r.license = :license")})
public class RadiationCenters implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "rad_id")
    private Long radId;
    @Column(name = "rad_name")
    private String radName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "mobile_no")
    private String mobileNo;
    @Column(name = "add_mobile_no")
    private String addMobileNo;
    @Column(name = "government")
    private String government;
    @Column(name = "area")
    private String area;
    @Column(name = "address")
    private String address;
    @Column(name = "national_id")
    private String nationalId;
    @Column(name = "license")
    private String license;
    @Column(name = "picture")
    private String picture;

    @JoinColumn(name = "rad_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    @OneToMany(mappedBy = "radioligyCenterId")
    private List<Radiology> radiologyList;
    public RadiationCenters() {
    }

    public RadiationCenters(Long radId) {
        this.radId = radId;
    }

    public Long getRadId() {
        return radId;
    }

    public void setRadId(Long radId) {
        this.radId = radId;
    }

    public String getRadName() {
        return radName;
    }

    public void setRadName(String radName) {
        this.radName = radName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddMobileNo() {
        return addMobileNo;
    }

    public void setAddMobileNo(String addMobileNo) {
        this.addMobileNo = addMobileNo;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public List<Radiology> getRadiologyList() {
        return radiologyList;
    }

    public void setRadiologyList(List<Radiology> radiologyList) {
        this.radiologyList = radiologyList;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (radId != null ? radId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RadiationCenters)) {
            return false;
        }
        RadiationCenters other = (RadiationCenters) object;
        if ((this.radId == null && other.radId != null) || (this.radId != null && !this.radId.equals(other.radId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.RadiationCenters[ radId=" + radId + " ]";
    }
    
}
