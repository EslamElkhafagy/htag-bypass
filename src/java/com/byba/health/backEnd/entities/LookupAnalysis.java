/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "lookup_analysis")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "LookupAnalysis.findAll", query = "SELECT l FROM LookupAnalysis l"),
    @NamedQuery(name = "LookupAnalysis.findById", query = "SELECT l FROM LookupAnalysis l WHERE l.id = :id"),
    @NamedQuery(name = "LookupAnalysis.findByName", query = "SELECT l FROM LookupAnalysis l WHERE l.name = :name"),
    @NamedQuery(name = "LookupAnalysis.findByTypeDoctorCustom", query = "SELECT l FROM LookupAnalysis l WHERE l.typeDoctorCustom = :typeDoctorCustom"),
    @NamedQuery(name = "LookupAnalysis.findByNotes", query = "SELECT l FROM LookupAnalysis l WHERE l.notes = :notes")})
public class LookupAnalysis implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "name")
    private String name;
    @Column(name = "type_doctor_custom")
    private String typeDoctorCustom;
    @Column(name = "notes")
    private String notes;

    public LookupAnalysis() {
    }

    public LookupAnalysis(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTypeDoctorCustom() {
        return typeDoctorCustom;
    }

    public void setTypeDoctorCustom(String typeDoctorCustom) {
        this.typeDoctorCustom = typeDoctorCustom;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof LookupAnalysis)) {
            return false;
        }
        LookupAnalysis other = (LookupAnalysis) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.LookupAnalysis[ id=" + id + " ]";
    }
    
}
