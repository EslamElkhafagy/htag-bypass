/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Abdelrahman
 */
@Entity
@Table(name = "appointments")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Appointments.findAll", query = "SELECT a FROM Appointments a"),
    @NamedQuery(name = "Appointments.findByAppId", query = "SELECT a FROM Appointments a WHERE a.appId = :appId"),
    @NamedQuery(name = "Appointments.findByDay", query = "SELECT a FROM Appointments a WHERE a.day = :day"),
    @NamedQuery(name = "Appointments.findByFrom", query = "SELECT a FROM Appointments a WHERE a.from = :from"),
    @NamedQuery(name = "Appointments.findByTo", query = "SELECT a FROM Appointments a WHERE a.to = :to")})
public class Appointments implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "app_id")
    private Integer appId;
    @Column(name = "day")
    @Temporal(TemporalType.DATE)
    private Date day;
    @Column(name = "from")
    @Temporal(TemporalType.TIME)
    private Date from;
    @Column(name = "to")
    @Temporal(TemporalType.TIME)
    private Date to;
    @OneToMany(mappedBy = "appointmentId")
    private List<Clinics> clinicsList;

    public Appointments() {
    }

    public Appointments(Integer appId) {
        this.appId = appId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Date getFrom() {
        return from;
    }

    public void setFrom(Date from) {
        this.from = from;
    }

    public Date getTo() {
        return to;
    }

    public void setTo(Date to) {
        this.to = to;
    }

    @XmlTransient
    public List<Clinics> getClinicsList() {
        return clinicsList;
    }

    public void setClinicsList(List<Clinics> clinicsList) {
        this.clinicsList = clinicsList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (appId != null ? appId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointments)) {
            return false;
        }
        Appointments other = (Appointments) object;
        if ((this.appId == null && other.appId != null) || (this.appId != null && !this.appId.equals(other.appId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Appointments[ appId=" + appId + " ]";
    }
    
}
