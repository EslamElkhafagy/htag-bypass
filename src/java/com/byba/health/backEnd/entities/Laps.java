/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Abdelrahman
 */
@Entity
@Table(name = "laps")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Laps.findAll", query = "SELECT l FROM Laps l"),
        @NamedQuery(name = "Laps.findByLapId", query = "SELECT l FROM Laps l WHERE l.lapId = :lapId"),
        @NamedQuery(name = "Laps.findByLapName", query = "SELECT l FROM Laps l WHERE l.lapName = :lapName"),
        @NamedQuery(name = "Laps.findByEmail", query = "SELECT l FROM Laps l WHERE UPPER(l.email) LIKE UPPER(:email)"),
        @NamedQuery(name = "Laps.findByPassword", query = "SELECT l FROM Laps l WHERE l.password = :password"),
        @NamedQuery(name = "Laps.findByMobileNo", query = "SELECT l FROM Laps l WHERE l.mobileNo = :mobileNo"),
        @NamedQuery(name = "Laps.findByAddMobileNo", query = "SELECT l FROM Laps l WHERE l.addMobileNo = :addMobileNo"),
        @NamedQuery(name = "Laps.findByGovernment", query = "SELECT l FROM Laps l WHERE l.government = :government"),
        @NamedQuery(name = "Laps.findByArea", query = "SELECT l FROM Laps l WHERE l.area = :area"),
        @NamedQuery(name = "Laps.findByAddress", query = "SELECT l FROM Laps l WHERE l.address = :address"),
        @NamedQuery(name = "Laps.findByNationalId", query = "SELECT l FROM Laps l WHERE l.nationalId = :nationalId"),
        @NamedQuery(name = "Laps.findByLicense", query = "SELECT l FROM Laps l WHERE l.license = :license")})
public class Laps implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "lap_id")
    private Long lapId;
    @Column(name = "lap_name")
    private String lapName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "mobile_no")
    private String mobileNo;
    @Column(name = "add_mobile_no")
    private String addMobileNo;
    @Column(name = "government")
    private String government;
    @Column(name = "area")
    private String area;
    @Column(name = "address")
    private String address;
    @Column(name = "national_id")
    private String nationalId;
    @Column(name = "license")
    private String license;
    @Column(name = "picture")
    private String picture;

    @JoinColumn(name = "lap_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public Laps() {
    }

    public Laps(Long lapId) {
        this.lapId = lapId;
    }

    public Long getLapId() {
        return lapId;
    }

    public void setLapId(Long lapId) {
        this.lapId = lapId;
    }

    public String getLapName() {
        return lapName;
    }

    public void setLapName(String lapName) {
        this.lapName = lapName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAddMobileNo() {
        return addMobileNo;
    }

    public void setAddMobileNo(String addMobileNo) {
        this.addMobileNo = addMobileNo;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getNationalId() {
        return nationalId;
    }

    public void setNationalId(String nationalId) {
        this.nationalId = nationalId;
    }

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (lapId != null ? lapId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Laps)) {
            return false;
        }
        Laps other = (Laps) object;
        if ((this.lapId == null && other.lapId != null) || (this.lapId != null && !this.lapId.equals(other.lapId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Laps[ lapId=" + lapId + " ]";
    }

}
