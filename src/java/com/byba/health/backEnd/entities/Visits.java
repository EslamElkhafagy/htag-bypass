/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * @author Abdelrahman
 */
@Entity
@Table(name = "visits")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Visits.findAll", query = "SELECT v FROM Visits v"),
        @NamedQuery(name = "Visits.findByVisitId", query = "SELECT v FROM Visits v WHERE v.id = :visitId"),
        @NamedQuery(name = "Visits.findByPatientIdForDoctors", query = "SELECT v FROM Visits v WHERE v.patientId = :patientId and v.clinicId = :clinicId"),
        @NamedQuery(name = "Visits.findByPatientId", query = "SELECT v FROM Visits v WHERE v.patientId = :patientId order by v.date desc"),
        @NamedQuery(name = "Visits.findLatestByPatientId", query = "SELECT v FROM Visits v WHERE v.patientId = :patientId and v.clinicId = :clinicId order by v.date desc"),
        @NamedQuery(name = "Visits.findByDate", query = "SELECT v FROM Visits v WHERE v.date = :date"),
        @NamedQuery(name = "Visits.findPatientsByDoctors", query = "SELECT distinct v.patientId FROM Visits v WHERE v.clinicId.doctorId.doctors.doctorId = :doctorId"),
        @NamedQuery(name = "Visits.findByNotes", query = "SELECT v FROM Visits v WHERE v.notes = :notes")})
public class Visits implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "visit_id")
    private Long id;
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Column(name = "notes")
    private String notes;

    @OneToMany(mappedBy = "visitId", fetch = FetchType.LAZY)
    private List<FollowUp> followUpList;
    @JoinColumn(name = "clinic_id", referencedColumnName = "clinic_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Clinics clinicId;
    @JoinColumn(name = "patient_id", referencedColumnName = "patient_id")
    @ManyToOne
    private Patient patientId;

    public Visits() {
    }
    public Visits(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Patient getPatientId() {
        return patientId;
    }

    public void setPatientId(Patient patientId) {
        this.patientId = patientId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @XmlTransient
    public List<FollowUp> getFollowUpList() {
        return followUpList;
    }

    public void setFollowUpList(List<FollowUp> followUpList) {
        this.followUpList = followUpList;
    }

    public Clinics getClinicId() {
        return clinicId;
    }

    public void setClinicId(Clinics clinicId) {
        this.clinicId = clinicId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Visits)) {
            return false;
        }
        Visits other = (Visits) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Visits[ visitId=" + id + " ]";
    }

}
