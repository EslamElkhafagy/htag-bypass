/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "patient")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Patient.findAll", query = "SELECT p FROM Patient p")
        , @NamedQuery(name = "Patient.findByPatientId", query = "SELECT p FROM Patient p WHERE p.patientId = :patientId")
        , @NamedQuery(name = "Patient.findByPatientName", query = "SELECT p FROM Patient p WHERE p.patientName = :patientName")
        , @NamedQuery(name = "Patient.deleteByCardId", query = "delete FROM Patient p WHERE p.cardId = :cardId")
        , @NamedQuery(name = "Patient.findByEmail", query = "SELECT p FROM Patient p WHERE UPPER(p.email) LIKE UPPER(:email)")
        , @NamedQuery(name = "Patient.findByPassword", query = "SELECT p FROM Patient p WHERE p.password = :password")
        , @NamedQuery(name = "Patient.findByMobileNumber", query = "SELECT p FROM Patient p WHERE p.mobileNumber = :mobileNumber")
        , @NamedQuery(name = "Patient.findByDateOfBirth", query = "SELECT p FROM Patient p WHERE p.dateOfBirth = :dateOfBirth")
        , @NamedQuery(name = "Patient.findByNationalId", query = "SELECT p FROM Patient p WHERE p.nationalId = :nationalId")
        , @NamedQuery(name = "Patient.findByGender", query = "SELECT p FROM Patient p WHERE p.gender = :gender")
        , @NamedQuery(name = "Patient.findByRelativeMobileNumber", query = "SELECT p FROM Patient p WHERE p.relativeMobileNumber = :relativeMobileNumber")
        , @NamedQuery(name = "Patient.findByGovernment", query = "SELECT p FROM Patient p WHERE p.government = :government")
        , @NamedQuery(name = "Patient.findByArea", query = "SELECT p FROM Patient p WHERE p.area = :area")
        , @NamedQuery(name = "Patient.findByAddress", query = "SELECT p FROM Patient p WHERE p.address = :address")
        , @NamedQuery(name = "Patient.findByMaritalStatus", query = "SELECT p FROM Patient p WHERE p.maritalStatus = :maritalStatus")
        , @NamedQuery(name = "Patient.findByOffspring", query = "SELECT p FROM Patient p WHERE p.offspring = :offspring")
        , @NamedQuery(name = "Patient.findByChronicDiseases", query = "SELECT p FROM Patient p WHERE p.chronicDiseases = :chronicDiseases")
        , @NamedQuery(name = "Patient.findByPresentMedications", query = "SELECT p FROM Patient p WHERE p.presentMedications = :presentMedications")
        , @NamedQuery(name = "Patient.findByAllergien", query = "SELECT p FROM Patient p WHERE p.allergien = :allergien")
        , @NamedQuery(name = "Patient.findBySurgeries", query = "SELECT p FROM Patient p WHERE p.surgeries = :surgeries")
        , @NamedQuery(name = "Patient.findByBloodGroup", query = "SELECT p FROM Patient p WHERE p.bloodGroup = :bloodGroup")
        , @NamedQuery(name = "Patient.findByCardId", query = "SELECT p FROM Patient p WHERE p.cardId = :cardId")
        , @NamedQuery(name = "Patient.findByPicture", query = "SELECT p FROM Patient p WHERE p.picture = :picture")})
public class Patient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "patient_id")
    private Long patientId;
    @Column(name = "patient_name")
    private String patientName;
    @Column(name = "email")
    private String email;
    @Column(name = "password")
    private String password;
    @Column(name = "smoker")
    private String smoker;
    @Column(name = "hypertension")
    private String hypertension;
    @Column(name = "diabetes")
    private String diabetes;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @Column(name = "date_of_birth")
    private String dateOfBirth;
    @Column(name = "national_id")
    private BigInteger nationalId;
    @Column(name = "gender")
    private String gender;
    @Column(name = "relative_mobile_number")
    private String relativeMobileNumber;
    @Column(name = "government")
    private String government;
    @Column(name = "area")
    private String area;
    @Column(name = "address")
    private String address;
    @Column(name = "marital_status")
    private String maritalStatus;
    @Column(name = "offspring")
    private Integer offspring;
    @Column(name = "chronic_diseases")
    private String chronicDiseases;
    @Column(name = "present_medications")
    private String presentMedications;
    @Column(name = "allergien")
    private String allergien;
    @Column(name = "surgeries")
    private String surgeries;
    @Column(name = "blood_group")
    private String bloodGroup;
    @Column(name = "picture")
    private String picture;
    //    @Column(name = "verifyCode")
//    private String verifyCode;
    @OneToMany(mappedBy = "patientId")
    private List<Visits> visitsList;
    @JoinColumn(name = "card_id", referencedColumnName = "card_id")
    @ManyToOne
    private QrCards cardId;
    @JoinColumn(name = "patient_id", referencedColumnName = "user_id", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private UserProfile userProfile;

    public Patient() {
    }

    public Patient(Long patientId) {
        this.patientId = patientId;
    }

    public Long getPatientId() {
        return patientId;
    }

    public void setPatientId(Long patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public BigInteger getNationalId() {
        return nationalId;
    }

    public void setNationalId(BigInteger nationalId) {
        this.nationalId = nationalId;
    }

    public String getGender() {
        return gender;
    }

    public String getSmoker() {
        return smoker;
    }

    public void setSmoker(String smoker) {
        this.smoker = smoker;
    }

    public String getHypertension() {
        return hypertension;
    }

    public void setHypertension(String hypertension) {
        this.hypertension = hypertension;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getRelativeMobileNumber() {
        return relativeMobileNumber;
    }

    public void setRelativeMobileNumber(String relativeMobileNumber) {
        this.relativeMobileNumber = relativeMobileNumber;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Integer getOffspring() {
        return offspring;
    }

    public void setOffspring(Integer offspring) {
        this.offspring = offspring;
    }

    public String getChronicDiseases() {
        return chronicDiseases;
    }

    public void setChronicDiseases(String chronicDiseases) {
        this.chronicDiseases = chronicDiseases;
    }

    public String getPresentMedications() {
        return presentMedications;
    }

    public void setPresentMedications(String presentMedications) {
        this.presentMedications = presentMedications;
    }

    public String getAllergien() {
        return allergien;
    }

    public void setAllergien(String allergien) {
        this.allergien = allergien;
    }

    public String getSurgeries() {
        return surgeries;
    }

    public void setSurgeries(String surgeries) {
        this.surgeries = surgeries;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

//    public String getVerifyCode() {
//        return verifyCode;
//    }
//
//    public void setVerifyCode(String verifyCode) {
//        this.verifyCode = verifyCode;
//    }

    @XmlTransient
    public List<Visits> getVisitsList() {
        return visitsList;
    }

    public void setVisitsList(List<Visits> visitsList) {
        this.visitsList = visitsList;
    }

    public QrCards getCardId() {
        return cardId;
    }

    public void setCardId(QrCards cardId) {
        this.cardId = cardId;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (patientId != null ? patientId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Patient)) {
            return false;
        }
        Patient other = (Patient) object;
        if ((this.patientId == null && other.patientId != null) || (this.patientId != null && !this.patientId.equals(other.patientId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Patient[ patientId=" + patientId + " ]";
    }

}
