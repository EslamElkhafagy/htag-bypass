/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.*;
import javax.persistence.criteria.Fetch;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "user_profile")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "UserProfile.findAll", query = "SELECT u FROM UserProfile u")
        , @NamedQuery(name = "UserProfile.findByUserPassword", query = "SELECT u FROM UserProfile u WHERE u.userPassword = :userPassword")
        , @NamedQuery(name = "UserProfile.findByUserId", query = "SELECT u FROM UserProfile u WHERE u.userId = :userId")
        , @NamedQuery(name = "UserProfile.findByDefaultPassword", query = "SELECT u FROM UserProfile u WHERE u.defaultPassword = :defaultPassword")
        , @NamedQuery(name = "UserProfile.findByLoginName", query = "SELECT u FROM UserProfile u WHERE UPPER(u.loginName) LIKE UPPER(:loginName)")
        , @NamedQuery(name = "UserProfile.findByType", query = "SELECT u FROM UserProfile u WHERE u.type = :type")
        , @NamedQuery(name = "UserProfile.findByMobileNumber", query = "SELECT u FROM UserProfile u WHERE u.mobileNumber = :mobileNumber")})
public class UserProfile implements Serializable {

    private static final long serialVersionUID = 1L;
    @Column(name = "user_password")
    private String userPassword;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "default_password")
    private String defaultPassword;
    @Column(name = "login_name")
    private String loginName;
    @Column(name = "type")
    private String type;
    @Column(name = "mobile_number")
    private String mobileNumber;
    @ManyToMany(mappedBy = "userProfileList")
    private List<Roles> rolesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "userId")
    private List<Attachments> attachmentsList;
    @OneToMany(mappedBy = "assitantId")
    private List<Clinics> clinicsList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "doctorId")
    private List<Clinics> clinicsList1;
    @OneToMany(mappedBy = "secId")
    private List<Clinics> clinicsList2;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Assistant assistant;
    @OneToMany(mappedBy = "docId")
    private List<Assistant> assistantList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Laps laps;
    @OneToMany(mappedBy = "doctorId")
    private List<Secretary> secretaryList;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Secretary secretary;
    @OneToOne(fetch = FetchType.EAGER , cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Doctors doctors;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Patient patient;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private UserLogin userLogin;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private Pharmacies pharmacies;
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "userProfile")
    private RadiationCenters radiationCenters;

    // sessions_id table
    @OneToMany(mappedBy = "userProfile")
    private List<Sessions> sessions;



    @Column(name = "creation_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date creationDate;

    public UserProfile() {
    }

    public UserProfile(Long userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getDefaultPassword() {
        return defaultPassword;
    }

    public void setDefaultPassword(String defaultPassword) {
        this.defaultPassword = defaultPassword;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @XmlTransient
    public List<Roles> getRolesList() {
        return rolesList;
    }

    public void setRolesList(List<Roles> rolesList) {
        this.rolesList = rolesList;
    }

    @XmlTransient
    public List<Attachments> getAttachmentsList() {
        return attachmentsList;
    }

    public void setAttachmentsList(List<Attachments> attachmentsList) {
        this.attachmentsList = attachmentsList;
    }

    @XmlTransient
    public List<Clinics> getClinicsList() {
        return clinicsList;
    }

    public void setClinicsList(List<Clinics> clinicsList) {
        this.clinicsList = clinicsList;
    }

    @XmlTransient
    public List<Clinics> getClinicsList1() {
        return clinicsList1;
    }

    public void setClinicsList1(List<Clinics> clinicsList1) {
        this.clinicsList1 = clinicsList1;
    }

    @XmlTransient
    public List<Clinics> getClinicsList2() {
        return clinicsList2;
    }

    public void setClinicsList2(List<Clinics> clinicsList2) {
        this.clinicsList2 = clinicsList2;
    }

    public Assistant getAssistant() {
        return assistant;
    }

    public void setAssistant(Assistant assistant) {
        this.assistant = assistant;
    }

    @XmlTransient
    public List<Assistant> getAssistantList() {
        return assistantList;
    }

    public void setAssistantList(List<Assistant> assistantList) {
        this.assistantList = assistantList;
    }

    public Laps getLaps() {
        return laps;
    }

    public void setLaps(Laps laps) {
        this.laps = laps;
    }

    @XmlTransient
    public List<Secretary> getSecretaryList() {
        return secretaryList;
    }

    public void setSecretaryList(List<Secretary> secretaryList) {
        this.secretaryList = secretaryList;
    }

    public Secretary getSecretary() {
        return secretary;
    }

    public void setSecretary(Secretary secretary) {
        this.secretary = secretary;
    }

    public Doctors getDoctors() {
        return doctors;
    }

    public void setDoctors(Doctors doctors) {
        this.doctors = doctors;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public UserLogin getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(UserLogin userLogin) {
        this.userLogin = userLogin;
    }

    public Pharmacies getPharmacies() {
        return pharmacies;
    }

    public void setPharmacies(Pharmacies pharmacies) {
        this.pharmacies = pharmacies;
    }

    public RadiationCenters getRadiationCenters() {
        return radiationCenters;
    }

    public void setRadiationCenters(RadiationCenters radiationCenters) {
        this.radiationCenters = radiationCenters;
    }

    @XmlTransient
    public List<Sessions> getSessions() {
        return sessions;
    }

    public void setSessions(List<Sessions> sessions) {
        this.sessions = sessions;
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userId != null ? userId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserProfile)) {
            return false;
        }
        UserProfile other = (UserProfile) object;
        if ((this.userId == null && other.userId != null) || (this.userId != null && !this.userId.equals(other.userId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.UserProfile[ userId=" + userId + " ]";
    }

}
