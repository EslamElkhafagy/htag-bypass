/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Administrator
 */
@Entity
@Table(name = "cardslookup")
@XmlRootElement
@NamedQueries({
        @NamedQuery(name = "Cardslookup.findAll", query = "SELECT c FROM Cardslookup c")
        , @NamedQuery(name = "Cardslookup.findById", query = "SELECT c FROM Cardslookup c WHERE c.id = :id")
        , @NamedQuery(name = "Cardslookup.findByCardcode", query = "SELECT c FROM Cardslookup c WHERE UPPER(c.cardcode) LIKE UPPER(:cardcode)")
        , @NamedQuery(name = "Cardslookup.findByCardnumber", query = "SELECT c FROM Cardslookup c WHERE UPPER(c.cardnumber) LIKE UPPER(:cardNumber)")
        , @NamedQuery(name = "Cardslookup.findByPincode", query = "SELECT c FROM Cardslookup c WHERE c.pincode = :pincode")
        , @NamedQuery(name = "Cardslookup.findByPinAndNumber", query = "SELECT c FROM Cardslookup c WHERE UPPER(c.pincode) LIKE UPPER(:pincode) and  UPPER(c.cardnumber) LIKE UPPER(:cardnumber)")
        , @NamedQuery(name = "Cardslookup.findByUrl", query = "SELECT c FROM Cardslookup c WHERE c.url = :url")})
public class Cardslookup implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "cardcode")
    private String cardcode;
    @Column(name = "cardnumber")
    private String cardnumber;
    @Column(name = "pincode")
    private String pincode;
    @Column(name = "url")
    private String url;
    @Column(name = "used")
    private boolean used;

    public Cardslookup() {
    }

    public Cardslookup(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCardcode() {
        return cardcode;
    }

    public void setCardcode(String cardcode) {
        this.cardcode = cardcode;
    }

    public String getCardnumber() {
        return cardnumber;
    }

    public void setCardnumber(String cardnumber) {
        this.cardnumber = cardnumber;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed(boolean used) {
        this.used = used;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cardslookup)) {
            return false;
        }
        Cardslookup other = (Cardslookup) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.byba.health.backEnd.entities.Cardslookup[ id=" + id + " ]";
    }

}
