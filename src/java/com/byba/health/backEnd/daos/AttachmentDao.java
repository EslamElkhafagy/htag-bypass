package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.Attachments;
import com.byba.health.backEnd.entities.QrCards;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.backEnd.sessions.AttachmentBuisness;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class AttachmentDao extends AbstractDao<Attachments> {

    private EntityManager em;

    public AttachmentDao(EntityManager em) {
        super(Attachments.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<Attachments> findByUserId(UserProfile UserId) {
        return getEntityManager().createNamedQuery("Attachments.findByUserId").setParameter("UserId", UserId).getResultList();
    }

}
