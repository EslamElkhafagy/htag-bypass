package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Laps;
import com.byba.health.middleWare.dto.LapSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class LapsDao extends AbstractDao<Laps> {

    private EntityManager em;

    public LapsDao(EntityManager em) {
        super(Laps.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    public Laps getLapsByEmail(String mail) {

        Laps laps;
        Query query = em.createNamedQuery("Laps.findByEmail");
        query.setParameter("email", mail);
        try {
            laps = (Laps) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return laps;
    }

    public List<LapSearchDto> getLapsSearchData(SearchDto searchDto) {
        List<LapSearchDto>  lapSearchDto;
        String sql ="";
        if(searchDto.getGovernment() != null)
        {
            sql = "select l from  Laps l where  l.government = '"+searchDto.getGovernment()+"' ";
        }else{
            sql = "select l from  Laps l";
        }
        return (List<LapSearchDto>) getEntityManager().createQuery(sql).getResultList();

    }


}
