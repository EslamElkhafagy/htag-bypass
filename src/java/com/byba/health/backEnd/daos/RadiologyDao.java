package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.RadiationCenters;
import com.byba.health.backEnd.entities.Radiology;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class RadiologyDao extends AbstractDao<Radiology> {

    private EntityManager em;

    public RadiologyDao(EntityManager em) {
        super(Radiology.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    public List<Radiology> getRadiologyByFollow(FollowUp followUp) throws Exception{
        return getEntityManager().createNamedQuery("Radiology.findByFollow").setParameter("follow", followUp).getResultList();
    }
    public List<Radiology> getAllPatientRadiologies(Long patientId) throws Exception{
        return getEntityManager().createNamedQuery("Radiology.findByPatient").setParameter("patientId", patientId).getResultList();
    }
    public List<Radiology> getRadiologyByFollowAndStatus(FollowUp followUp , RadiationCenters radiationCenters) {
        return getEntityManager().createNamedQuery("Radiology.findByFollowAndStatus")
                .setParameter("follow", followUp)
                .setParameter("radId",radiationCenters).getResultList();
    }
    public List<Radiology> getCurrentRadiologies(RadiationCenters radiationCenters) {
        return getEntityManager().createNamedQuery("Radiology.findByCurrentTest")
                .setParameter("radId",radiationCenters).getResultList();
    }


}
