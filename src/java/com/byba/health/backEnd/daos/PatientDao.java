package com.byba.health.backEnd.daos;


import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.QrCards;
import com.byba.health.backEnd.entities.UserProfile;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class PatientDao extends AbstractDao<Patient> {

    private EntityManager em;

    public PatientDao(EntityManager em) {
        super(Patient.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Patient getPatientByLoginName(UserProfile patientId) {

        Patient patient;
        Query query = em.createNamedQuery("Patient.findByPatientId");
        query.setParameter("patientId", patientId);
        try {
            patient = (Patient) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return patient;
    }

    public Patient getPatientByQrCards(QrCards cardId) {

        Patient patient;
        Query query = em.createNamedQuery("Patient.findByCardId");
        query.setParameter("cardId", cardId);
        try {
            patient = (Patient) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return patient;
    }

    public List<Patient> getListPatientByQrCards(QrCards cardId) {

        List<Patient> patients;
        Query query = em.createNamedQuery("Patient.findByCardId");
        query.setParameter("cardId", cardId);
        try {
            patients = (List<Patient>) query.getResultList();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return patients;
    }

    public Patient getPatientById(Long id) {
        Patient patient;
        Query query = em.createNamedQuery("Patient.findByPatientId");
        query.setParameter("patientId", id);
        try {
            patient = (Patient) query.getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
            return null;
        }
        return patient;
    }

    public Patient getPatientByName(String patientName) {
        Patient patient;
        Query query = em.createNamedQuery("Patient.findByPatientName");
        query.setParameter("patientName", patientName);
        try {
            patient = (Patient) query.getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
            return null;
        }
        return patient;
    }

    public Long getCountOfCards() {
        Query query = em.createQuery(
                "select count(p.cardId) from Patient p  where p.cardId.isActive = true");
        return (Long) query.getSingleResult();
    }

    public BigInteger getCountOfCardPerDate(String date) {
        Query query = em.createNativeQuery("select count(patient.card_id) from patient inner join qr_cards on patient.card_id=qr_cards.card_id inner join user_profile on patient.patient_id=user_profile.user_id \n" +
                "where is_active = true and date_trunc ('day',creation_date) = '"+date+"'");
        return (BigInteger) query.getSingleResult();
    }

    public Patient getPatientByEmail(String email) {
        Patient patient;
        Query query = em.createNamedQuery("Patient.findByEmail");
        query.setParameter("email", email);
        try {
            patient = (Patient) query.getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
            return null;
        }
        return patient;
    }

}
