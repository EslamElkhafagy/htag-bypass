package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.Visits;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class VisitsDao extends AbstractDao<Visits> {

    private EntityManager em;

    public VisitsDao(EntityManager em) {
        super(Visits.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }
    /**
     *  MY Edit
     */
    public List<Visits> getALLVisitsByPatientIdForDoctors(Patient patientId) throws Exception {
        return getEntityManager().createNamedQuery("Visits.findByPatientId")
                .setParameter("patientId", patientId)
                .getResultList();
    }

    public List<Visits> getVisitsByPatientIdForDoctors(Patient patientId,Clinics clinics) throws Exception {
            return getEntityManager().createNamedQuery("Visits.findByPatientIdForDoctors")
                    .setParameter("patientId", patientId)
                    .setParameter("clinicId", clinics)
                    .getResultList();
    }
    public List<Patient> getPatientsByDoctors(Long doctorId) throws Exception {
            return getEntityManager().createNamedQuery("Visits.findPatientsByDoctors")
                    .setParameter("doctorId", doctorId)
                    .getResultList();
    }
    public List<Visits> getVisitsByPatientId(Patient patientId) throws Exception {
            return getEntityManager().createNamedQuery("Visits.findByPatientId")
                    .setParameter("patientId", patientId).getResultList();
    }
    public Visits getPatientLatestVisit(Patient patientId,Clinics clinics) throws Exception {
        List PatientLatestVisit = getEntityManager().createNamedQuery("Visits.findLatestByPatientId")
                .setParameter("patientId", patientId).setParameter("clinicId", clinics).getResultList();
        if (PatientLatestVisit.size()>0)
        return (Visits) PatientLatestVisit.get(0);
        else
            return null;

    }

}
