package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.Drugs;
import com.byba.health.backEnd.entities.FollowUp;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class DrugsDao extends AbstractDao<Drugs> {

    private EntityManager em;

    public DrugsDao(EntityManager em) {
        super(Drugs.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<Drugs> getDrugsByFollow(FollowUp followUp) throws Exception{
        return getEntityManager().createNamedQuery("Drugs.findByFollowUp").setParameter("followUp", followUp).getResultList();
    }

}
