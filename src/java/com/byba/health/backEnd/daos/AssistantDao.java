package com.byba.health.backEnd.daos;




import com.byba.health.backEnd.entities.Assistant;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class AssistantDao extends AbstractDao<Assistant> {

    private EntityManager em;

    public AssistantDao(EntityManager em) {
        super(Assistant.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
