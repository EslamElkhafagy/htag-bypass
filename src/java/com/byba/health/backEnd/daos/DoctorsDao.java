package com.byba.health.backEnd.daos;


import com.byba.health.backEnd.entities.Doctors;
import com.byba.health.backEnd.entities.UserProfile;

import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 * @author IbrahimShedid
 */

public class DoctorsDao extends AbstractDao<Doctors> {

    private EntityManager em;

    public DoctorsDao(EntityManager em) {
        super(Doctors.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Doctors getDoctorByLoginName(UserProfile doctorId) {

        Doctors doctor;
        Query query = em.createNamedQuery("Doctors.findByDoctorId");
        query.setParameter("doctorId", doctorId);
        try {
            doctor = (Doctors) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return doctor;
    }
    public Doctors getDoctorByName(String doctorName) {

        Doctors doctor;
        Query query = em.createNamedQuery("Doctors.findByDoctorName");
        query.setParameter("doctorName", doctorName);
        try {
            doctor = (Doctors) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return doctor;
    }
    public Doctors getDoctorByMail(String mail) {

        Doctors doctor;
        Query query = em.createNamedQuery("Doctors.findByEmail");
        query.setParameter("email", mail);
        try {
            doctor = (Doctors) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
        return doctor;
    }

}
