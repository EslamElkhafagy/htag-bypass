package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.QrCards;
import com.byba.health.backEnd.entities.Sessions;
import com.byba.health.backEnd.entities.UserProfile;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.List;

public class SessionsDao extends AbstractDao<Sessions> {

    private EntityManager em;

    public SessionsDao(EntityManager em) {
        super(Sessions.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public Sessions getsessionByUserId(UserProfile userId) {

        Sessions sessions;
        Query query = em.createNamedQuery("Sessions.findByUserID");
        query.setParameter("userId", userId);
        try {
            sessions = (Sessions) query.getSingleResult();
        } catch (Exception e) {
            System.out.println(e);
            return null;
        }
        return sessions;
    }

    public Sessions getSessionsBySessionValue(String session) {
        Sessions sessions;
        Query query = em.createNamedQuery("Sessions.findBySession");
        query.setParameter("session", session);
        try {
            sessions = (Sessions) query.getSingleResult();
        } catch (NonUniqueResultException | NoResultException e) {
            return null;
        }
        return sessions;
    }
}
