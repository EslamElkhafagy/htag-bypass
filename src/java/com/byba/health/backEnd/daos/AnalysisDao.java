package com.byba.health.backEnd.daos;



import com.byba.health.backEnd.entities.Analysis;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Laps;
import com.byba.health.backEnd.entities.Patient;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * @author IbrahimShedid
 */

public class AnalysisDao extends AbstractDao<Analysis> {

    private EntityManager em;

    public AnalysisDao(EntityManager em) {
        super(Analysis.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }

    public List<Analysis> getAnalysisByFollow(FollowUp followUp) throws Exception{
        return getEntityManager().createNamedQuery("Analysis.findByFollow").setParameter("follow", followUp).getResultList();
    }
    public List<Analysis> getAnalysisByPatient(Long patientId) throws Exception{
        return getEntityManager().createNamedQuery("Analysis.findByPatient").setParameter("patientId", patientId).getResultList();
    }
    public List<Analysis> getAnalysisByFollowAndStatus(FollowUp followUp , Laps lap) {
        return getEntityManager().createNamedQuery("Analysis.findByFollowAndStatus")
                .setParameter("follow", followUp)
                .setParameter("lapId",lap).getResultList();
    }
    public List<Analysis> getCurrentAnalysis(Laps lap) {
        return getEntityManager().createNamedQuery("Analysis.findByCurrentTest")
                .setParameter("lapId",lap).getResultList();
    }
    public Object sendNotificationToPatient(Patient patientId) {
        return getEntityManager().createNamedQuery("Analysis.getCountOfTestsNotFinished")
                .setParameter("patientId", patientId).getSingleResult();
    }
}
