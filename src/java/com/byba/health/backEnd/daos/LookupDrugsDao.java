package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.LookupDrugs;

import javax.persistence.EntityManager;

/**
 * @author IbrahimShedid
 */

public class LookupDrugsDao extends AbstractDao<LookupDrugs> {

    private EntityManager em;

    public LookupDrugsDao(EntityManager em) {
        super(LookupDrugs.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
