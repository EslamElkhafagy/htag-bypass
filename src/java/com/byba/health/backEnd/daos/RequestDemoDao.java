package com.byba.health.backEnd.daos;

import com.byba.health.backEnd.entities.RequestDemo;
import javax.persistence.EntityManager;
/**
 * @author IbrahimShedid
 */

public class RequestDemoDao extends AbstractDao<RequestDemo> {

    private EntityManager em;

    public RequestDemoDao(EntityManager em) {
        super(RequestDemo.class);
        setEm(em);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public void setEm(EntityManager em) {
        this.em = em;
    }


}
