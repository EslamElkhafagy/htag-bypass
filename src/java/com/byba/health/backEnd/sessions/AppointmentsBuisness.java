/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.AppointmentsDao;
import com.byba.health.backEnd.entities.Appointments;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class AppointmentsBuisness extends AbstractBusiness<Appointments> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private AppointmentsDao appointmentsDao;

    public AppointmentsBuisness() {
        super(Appointments.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Appointments> getAbstractDao() {
        if (appointmentsDao == null) {
            this.appointmentsDao = new AppointmentsDao(getEntityManager());
        }
        return appointmentsDao;
    }

    public AppointmentsDao getAppointmentsDao() {
        getAbstractDao();
        return appointmentsDao;
    }

}
