/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.VisitsDao;
import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.Visits;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class VisitsBuisness extends AbstractBusiness<Visits> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private VisitsDao visitsDao;

    public VisitsBuisness() {
        super(Visits.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Visits> getAbstractDao() {
        if (visitsDao == null) {
            this.visitsDao = new VisitsDao(getEntityManager());
        }
        return visitsDao;
    }

    public VisitsDao getVisitsDao() {
        getAbstractDao();
        return visitsDao;
    }
    public List<Patient> getPatientsByDoctors(Long doctorId) throws Exception{
            return getVisitsDao().getPatientsByDoctors(doctorId);
    }
    public List<Visits> getVisitsByPatientIdForDoctors(Patient patientId,Clinics clinics) throws Exception{
            return getVisitsDao().getVisitsByPatientIdForDoctors(patientId,clinics);
    }
    /**
     *  MY Edit
     */
    public List<Visits> getALLVisitsByPatientIdForDoctors(Patient patientId) throws Exception{
        return getVisitsDao().getALLVisitsByPatientIdForDoctors(patientId);
    }
    public List<Visits> getVisitsByPatientId(Patient patientId) throws Exception{
            return getVisitsDao().getVisitsByPatientId(patientId);
    }
    public Visits getPatientLatestVisit(Patient patientId,Clinics clinics) throws Exception{
            return getVisitsDao().getPatientLatestVisit(patientId,clinics);
    }

}
