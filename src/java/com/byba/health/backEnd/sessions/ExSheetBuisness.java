/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.ExSheetDao;
import com.byba.health.backEnd.entities.ExaminationSheet;
import com.byba.health.backEnd.entities.FollowUp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class ExSheetBuisness extends AbstractBusiness<ExaminationSheet> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private ExSheetDao exSheetDao;

    public ExSheetBuisness() {
        super(ExaminationSheet.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<ExaminationSheet> getAbstractDao() {
        if (exSheetDao == null) {
            this.exSheetDao = new ExSheetDao(getEntityManager());
        }
        return exSheetDao;
    }

    public ExSheetDao getExaminationSheetDao() {
        getAbstractDao();
        return exSheetDao;
    }
    public List<ExaminationSheet> getExaminationSheetByFollow(FollowUp followUp) throws Exception{
        return getExaminationSheetDao().getExaminationSheetByFollow(followUp);
    }

}
