package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.PatientDao;
import com.byba.health.backEnd.daos.SessionsDao;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.Sessions;
import org.eclipse.jetty.util.annotation.Name;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateless
public class SessionsBuisness extends AbstractBusiness<Sessions>{

    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;

    private SessionsDao sessionsDao;


    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public SessionsDao getSessionDao() {
        getAbstractDao();
        return sessionsDao;
    }

    public Sessions getSessionBySessionValue(String session) {
        return getSessionDao().getSessionsBySessionValue(session);
    }

    @Override
    protected AbstractDao<Sessions> getAbstractDao() {
        if (sessionsDao == null) {
            this.sessionsDao = new SessionsDao(getEntityManager());
        }
        return sessionsDao;
    }
}
