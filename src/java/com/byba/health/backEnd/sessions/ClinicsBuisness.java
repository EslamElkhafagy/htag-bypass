/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.ClinicsDao;
import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.Doctors;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.middleWare.dto.SearchDto;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class ClinicsBuisness extends AbstractBusiness<Clinics> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private ClinicsDao clinicsDao;

    @EJB
    UserProfileBuisness userProfileBuisness;

    public ClinicsBuisness() {
        super(Clinics.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Clinics> getAbstractDao() {
        if (clinicsDao == null) {
            this.clinicsDao = new ClinicsDao(getEntityManager());
        }
        return clinicsDao;
    }

    public ClinicsDao getClinicsDao() {
        getAbstractDao();
        return clinicsDao;
    }

    @Override
    public void create(Clinics entity) {
        super.create(entity);
    }
    public List<Clinics> getClinicsByDoctor(UserProfile doctor) {
        return getClinicsDao().getClinicsByDoctor(doctor);
    }


}
