/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.FollowUpDao;
import com.byba.health.backEnd.entities.*;
import com.byba.health.middleWare.dto.LabSheetDto;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * @author IbrahimShedid
 */
@Stateless
public class FollowUpBuisness extends AbstractBusiness<FollowUp> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private FollowUpDao followUpDao;
    @EJB
    private PatientBuisness patientBuisness;
    @EJB
    private DrugsBuisness drugsBuisness;
    @EJB
    private RadiologyBuisness radiologyBuisness;
    @EJB
    private AnalysisBuisness analysisBuisness;
    @EJB
    private VisitsBuisness visitsBuisness;
    @EJB
    private ExSheetBuisness exSheetBuisness;
    @EJB
    private RadiationBuisness radiationBuisness;
    @EJB
    private LapsBuisness lapsBuisness;
    @EJB
    private ClinicsBuisness clinicsBuisness;

    public FollowUpBuisness() {
        super(FollowUp.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<FollowUp> getAbstractDao() {
        if (followUpDao == null) {
            this.followUpDao = new FollowUpDao(getEntityManager());
        }
        return followUpDao;
    }

    public FollowUpDao getFollowUpsDao() {
        getAbstractDao();
        return followUpDao;
    }

    public void saveFollowUpTreatment(FollowUp followUp) {
        List<Drugs> drugsList = followUp.getDrugsList();
        if (drugsList != null) {
            for (Drugs drugs : drugsList) {
                drugs.setFollow(followUp);
                drugsBuisness.edit(drugs);
            }
        }

    }

    public void saveFollowUpAnalysis(FollowUp followUp) {
        List<Analysis> analysisList = followUp.getAnalysisList();
        if (analysisList != null) {
            for (Analysis analysis : analysisList) {
                analysis.setFollow(followUp);
                analysisBuisness.edit(analysis);
            }
        }

    }

    public void saveFollowUpRadiologies(FollowUp followUp) {
        List<Radiology> radiologyList = followUp.getRadiologyList();
        if (radiologyList != null) {
            for (Radiology radiology : radiologyList) {
                radiology.setFollow(followUp);
                radiologyBuisness.edit(radiology);
            }
        }

    }

    public void addExaminationSheet(FollowUp followUp) {
        Visits visitId = followUp.getVisitId();
//        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//
//        followUp.setDate(timestamp);
//        visitId.setDate(timestamp);
        if (visitId.getId() == null) {
            visitId.setDate(followUp.getDate());
            visitsBuisness.create(visitId);
        }else {
            visitId = visitsBuisness.find(visitId.getId());
        }
        followUp.setVisitId(visitId);
        List<Drugs> drugsList = followUp.getDrugsList();
        List<Analysis> analysisList = followUp.getAnalysisList();
        List<Radiology> radiologyList = followUp.getRadiologyList();
        List<ExaminationSheet> examinationSheetList = followUp.getExaminationSheetList();
        if (examinationSheetList != null) {
            for (ExaminationSheet examinationSheet : examinationSheetList) {
                examinationSheet.setFollow(followUp);
            }
        }
        if (drugsList != null) {
            for (Drugs drugs : drugsList) {
                drugs.setFollow(followUp);
            }
        }
        if (analysisList != null) {
            for (Analysis analysis : analysisList) {
                analysis.setFollow(followUp);
            }
        }
        if (radiologyList != null) {
            for (Radiology radiology : radiologyList) {
                radiology.setFollow(followUp);
            }
        }

        create(followUp);

    }
    public void addExaminationSheetForLab(LabSheetDto labSheetDto) throws Exception {
        FollowUp followUp = labSheetDto.getFollowUp();
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Laps lap = lapsBuisness.find(labSheetDto.getLapId());
        UserProfile userProfile = lap.getUserProfile();
        List<Clinics> clinicsByDoctor = clinicsBuisness.getClinicsByDoctor(userProfile);
        followUp.setDate(timestamp);
        Visits visitId = new Visits();
            visitId.setDate(timestamp);
            visitId.setPatientId(patientBuisness.find(labSheetDto.getPatientid()));
            visitId.setClinicId(clinicsByDoctor.get(0));
            visitsBuisness.create(visitId);

        followUp.setVisitId(visitId);
        List<Analysis> analysisList = followUp.getAnalysisList();
        if (analysisList != null) {
            for (Analysis analysis : analysisList) {
                analysis.setLapId(lap);
                analysis.setStatus("InProgress");
                analysis.setFollow(followUp);
            }
        }

        create(followUp);

    }

    public Object getAttachment(Long id, Long number) {
        if (number == 1) {//Analysis
            Analysis analysis = analysisBuisness.find(id);
            return analysis;
        } else {//Radiology
            Radiology radiology = radiologyBuisness.find(id);
            return radiology;
        }
    }

    public List<FollowUp> getFollowUpsByVisits(List<Visits> visits) {
        List<FollowUp> followUpByVisits = null;
        try {
            followUpByVisits = getFollowUpsDao().getFollowUpByVisits(visits);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return followUpByVisits;
    }

    public List<FollowUp> getFollowUpsByVisit(Visits visit) {
        List<FollowUp> followUpByVisits = null;
        try {
            followUpByVisits = getFollowUpsDao().getFollowUpByVisit(visit);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return followUpByVisits;
    }

    public List<Visits> getPatientVisits(Long patientId) throws Exception {
        Patient patientById = patientBuisness.getPatientById(patientId);
        List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientId(patientById);
        if (visitsByPatientId != null) {
            for (Visits visit : visitsByPatientId) {
                List<FollowUp> followUpsByVisit = getFollowUpsByVisit(visit);
                for (FollowUp followUp : followUpsByVisit) {
                    List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                    followUp.setDrugsList(drugsByFollow);

                    List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                    followUp.setRadiologyList(radiologyByFollow);

                    List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                    followUp.setAnalysisList(analysisByFollow);

                    List<ExaminationSheet> examinationSheetByFollow = exSheetBuisness.getExaminationSheetByFollow(followUp);
                    followUp.setExaminationSheetList(examinationSheetByFollow);


                }
                visit.setFollowUpList(followUpsByVisit);

            }
        }
        return visitsByPatientId;
    }

    public Visits getPatientLatestVisit(Long patientId, Integer clinicId) throws Exception {
        Clinics clinics = clinicsBuisness.find(clinicId);
        Patient patientById = patientBuisness.getPatientById(patientId);
        Visits patientLatestVisit = visitsBuisness.getPatientLatestVisit(patientById, clinics);
        if (patientLatestVisit != null) {
            List<FollowUp> followUpsByVisit = getFollowUpsByVisit(patientLatestVisit);
            for (FollowUp followUp : followUpsByVisit) {
                List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                followUp.setDrugsList(drugsByFollow);

                List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                followUp.setRadiologyList(radiologyByFollow);

                List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                followUp.setAnalysisList(analysisByFollow);

                List<ExaminationSheet> examinationSheetByFollow = exSheetBuisness.getExaminationSheetByFollow(followUp);
                followUp.setExaminationSheetList(examinationSheetByFollow);

            }
            patientLatestVisit.setFollowUpList(followUpsByVisit);


        }
        return patientLatestVisit;
    }

    public List<FollowUp> getPatientLatestFollows(Long patientId, Integer clinicId) throws Exception {
        Clinics clinics = clinicsBuisness.find(clinicId);

        Patient patientById = patientBuisness.getPatientById(patientId);
        Visits patientLatestVisit = visitsBuisness.getPatientLatestVisit(patientById, clinics);
        List<FollowUp> followUpsByVisit = new ArrayList<>();
        if (patientLatestVisit != null) {
            followUpsByVisit = getFollowUpsByVisit(patientLatestVisit);
            for (FollowUp followUp : followUpsByVisit) {

                followUp.setVisitId(patientLatestVisit);
                List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                followUp.setDrugsList(drugsByFollow);

                List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                followUp.setRadiologyList(radiologyByFollow);

                List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                followUp.setAnalysisList(analysisByFollow);

                List<ExaminationSheet> examinationSheetByFollow = exSheetBuisness.getExaminationSheetByFollow(followUp);
                followUp.setExaminationSheetList(examinationSheetByFollow);

            }

        }
        return followUpsByVisit;
    }
    /**
     *
     * My Edit part 2
     */
    public List<FollowUp> getAllFollowUpsByPatient(Long patientId) throws Exception {

        Patient patientById = patientBuisness.getPatientById(patientId);
        List<Visits> visitsByPatientId = visitsBuisness.getALLVisitsByPatientIdForDoctors(patientById);
        List<FollowUp> followUpsByVisits = getFollowUpsByVisits(visitsByPatientId);
        if (followUpsByVisits != null) {
            for (FollowUp followUp : followUpsByVisits) {

                List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                followUp.setDrugsList(drugsByFollow);

                List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                followUp.setRadiologyList(radiologyByFollow);

                List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                followUp.setAnalysisList(analysisByFollow);

                List<ExaminationSheet> examinationSheetByFollow = exSheetBuisness.getExaminationSheetByFollow(followUp);
                followUp.setExaminationSheetList(examinationSheetByFollow);


            }
        }
        return followUpsByVisits;
    }
    public List<FollowUp> getFollowUpsByPatient(Long patientId, Integer clinicId) throws Exception {
        Clinics clinics = clinicsBuisness.find(clinicId);
        Patient patientById = patientBuisness.getPatientById(patientId);
        List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientIdForDoctors(patientById, clinics);
        List<FollowUp> followUpsByVisits = getFollowUpsByVisits(visitsByPatientId);
        if (followUpsByVisits != null) {
            for (FollowUp followUp : followUpsByVisits) {

                List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                followUp.setDrugsList(drugsByFollow);

                List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                followUp.setRadiologyList(radiologyByFollow);

                List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                followUp.setAnalysisList(analysisByFollow);

                List<ExaminationSheet> examinationSheetByFollow = exSheetBuisness.getExaminationSheetByFollow(followUp);
                followUp.setExaminationSheetList(examinationSheetByFollow);


            }
        }
        return followUpsByVisits;
    }

    public List<Radiology> getCurrentRadiologies(Long radId) {
        RadiationCenters radiationCenters = radiationBuisness.find(radId);
        return radiologyBuisness.getCurrentRadiologies(radiationCenters);
    }

    public List<FollowUp> getRadFollowUpsByStatus(Long patientId, Long radId) throws Exception {
        Patient patientById = patientBuisness.getPatientById(patientId);
        RadiationCenters radiationCenters = radiationBuisness.find(radId);
        List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientId(patientById);
        List<FollowUp> followUpsByVisits = getFollowUpsByVisits(visitsByPatientId);
        if (followUpsByVisits != null) {
            for (FollowUp followUp : followUpsByVisits) {
                List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollowAndStatus(followUp, radiationCenters);
                followUp.setRadiologyList(radiologyByFollow);
            }
        }
        return followUpsByVisits;
    }

    public List<Analysis> getCurrentAnalysis(Long lapId) {
        Laps laps = lapsBuisness.find(lapId);
        return analysisBuisness.getCurrentAnalysis(laps);
    }

    public List<FollowUp> getAnalysisFollowUpsByStatus(Long patientId, Long lapId) throws Exception {
        Patient patientById = patientBuisness.getPatientById(patientId);
        Laps laps = lapsBuisness.find(lapId);
        List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientId(patientById);
        List<FollowUp> followUpsByVisits = getFollowUpsByVisits(visitsByPatientId);
        if (followUpsByVisits != null) {
            for (FollowUp followUp : followUpsByVisits) {
                List<Analysis> analysises = analysisBuisness.getAnalysisByFollowAndStatus(followUp, laps);
                followUp.setAnalysisList(analysises);
            }
        }
        return followUpsByVisits;
    }
    public List<Analysis> getAnalysisFollowUpsByPatient(Long patientId) throws Exception {
       return analysisBuisness.getAnalysisByPatient(patientId);
    }
    public List<Radiology> getAllPatientRadiologies(Long patientId) throws Exception {
       return radiologyBuisness.getAllPatientRadiologies(patientId);
    }

    public List<FollowUp> getFollowUpsByPatient(Long patientId, int type) throws Exception {
        Patient patientById = patientBuisness.getPatientById(patientId);
        List<Visits> visitsByPatientId = visitsBuisness.getVisitsByPatientId(patientById);
        List<FollowUp> followUpsByVisits = getFollowUpsByVisits(visitsByPatientId);
        if (followUpsByVisits != null) {
            for (FollowUp followUp : followUpsByVisits) {
                if (type == 1) {
                    List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
                    followUp.setDrugsList(drugsByFollow);
                }
                if (type == 2) {

                    List<Radiology> radiologyByFollow = radiologyBuisness.getRadiologyByFollow(followUp);
                    followUp.setRadiologyList(radiologyByFollow);
                }
                if (type == 3) {

                    List<Analysis> analysisByFollow = analysisBuisness.getAnalysisByFollow(followUp);
                    followUp.setAnalysisList(analysisByFollow);
                }
            }
        }
        return followUpsByVisits;
    }
    public Analysis getAnalysisById(Long id) throws Exception {
        return analysisBuisness.find(id);
    }
    public Radiology getRadiologyById(Long id) throws Exception {
        return radiologyBuisness.find(id);
    }
    public FollowUp getFollowById(Long id) throws Exception {
        FollowUp followUp = find(id);
        List<Drugs> drugsByFollow = drugsBuisness.getDrugsByFollow(followUp);
        followUp.setDrugsList(drugsByFollow);

        return followUp;
    }

}
