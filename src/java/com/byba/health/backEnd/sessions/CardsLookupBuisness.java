/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.CardsLookupDao;
import com.byba.health.backEnd.entities.Cardslookup;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class CardsLookupBuisness extends AbstractBusiness<Cardslookup> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private CardsLookupDao cardsLookupDao;

    public CardsLookupBuisness() {
        super(Cardslookup.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Cardslookup> getAbstractDao() {
        if (cardsLookupDao == null) {
            this.cardsLookupDao = new CardsLookupDao(getEntityManager());
        }
        return cardsLookupDao;
    }

    public CardsLookupDao getCardsLookupDao() {
        getAbstractDao();
        return cardsLookupDao;
    }
    public Cardslookup getCardByNumber(String number) {
        return getCardsLookupDao().getCardByNumber(number);
    }
    public Cardslookup getCardByPin(String Pin) {
        return getCardsLookupDao().getCardByPin(Pin);
    }
    public Cardslookup getCardByPinAndNumber(Cardslookup cardslookup) {
        return getCardsLookupDao().getCardByPinAndNumber(cardslookup);
    }

}
