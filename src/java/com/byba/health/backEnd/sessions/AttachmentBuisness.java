/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.AttachmentDao;
import com.byba.health.backEnd.entities.Attachments;
import com.byba.health.backEnd.entities.UserProfile;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class AttachmentBuisness extends AbstractBusiness<Attachments> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private AttachmentDao attachmentDao;

    public AttachmentBuisness() {
        super(Attachments.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Attachments> getAbstractDao() {
        if (attachmentDao == null) {
            this.attachmentDao = new AttachmentDao(getEntityManager());
        }
        return attachmentDao;
    }

    public AttachmentDao getAttachmentDao() {
        getAbstractDao();
        return attachmentDao;
    }
    public List<Attachments> findByUserId(UserProfile UserId) {
        return getAttachmentDao().findByUserId(UserId);
    }

}
