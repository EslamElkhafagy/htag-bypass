/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.UserLoginDao;
import com.byba.health.backEnd.entities.UserLogin;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class UserLoginBuisness extends AbstractBusiness<UserLogin> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private UserLoginDao userLoginDao;

    public UserLoginBuisness() {
        super(UserLogin.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<UserLogin> getAbstractDao() {
        if (userLoginDao == null) {
            this.userLoginDao = new UserLoginDao(getEntityManager());
        }
        return userLoginDao;
    }

    public UserLoginDao getUserLoginDao() {
        getAbstractDao();
        return userLoginDao;
    }

}
