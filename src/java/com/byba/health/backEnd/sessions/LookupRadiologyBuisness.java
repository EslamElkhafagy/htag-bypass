/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.LookupRadiologyDao;
import com.byba.health.backEnd.entities.LookupRadiology;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class LookupRadiologyBuisness extends AbstractBusiness<LookupRadiology> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private LookupRadiologyDao lookupRadiologyDao;

    public LookupRadiologyBuisness() {
        super(LookupRadiology.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<LookupRadiology> getAbstractDao() {
        if (lookupRadiologyDao == null) {
            this.lookupRadiologyDao = new LookupRadiologyDao(getEntityManager());
        }
        return lookupRadiologyDao;
    }

    public LookupRadiologyDao getAppointmentsDao() {
        getAbstractDao();
        return lookupRadiologyDao;
    }

}
