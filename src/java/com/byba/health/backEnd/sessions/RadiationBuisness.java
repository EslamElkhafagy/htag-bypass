/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.RadiationsDao;
import com.byba.health.backEnd.entities.RadiationCenters;
import com.byba.health.middleWare.dto.RadSearchDto;
import com.byba.health.middleWare.dto.SearchDto;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class RadiationBuisness extends AbstractBusiness<RadiationCenters> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private RadiationsDao radiationsDao;

    public RadiationBuisness() {
        super(RadiationCenters.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<RadiationCenters> getAbstractDao() {
        if (radiationsDao == null) {
            this.radiationsDao = new RadiationsDao(getEntityManager());
        }
        return radiationsDao;
    }

    public RadiationsDao getRadiationsDao() {
        getAbstractDao();
        return radiationsDao;
    }
    public RadiationCenters getRadiationsByEmail(String mail) {
        return getRadiationsDao().getRadiationsByEmail(mail);
    }
    public List<RadSearchDto> getRadsSearchData(SearchDto searchDto) {
        return getRadiationsDao().getRadsSearchData(searchDto);
    }

}
