/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.AnalysisDao;
import com.byba.health.backEnd.entities.Analysis;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Laps;
import com.byba.health.backEnd.utils.Utils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Stateless
public class AnalysisBuisness extends AbstractBusiness<Analysis> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private AnalysisDao analysisDao;

    @EJB
    private VisitsBuisness visitsBuisness;
    @EJB
    private FollowUpBuisness followUpBuisness;

    public AnalysisBuisness() {
        super(Analysis.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<Analysis> getAbstractDao() {
        if (analysisDao == null) {
            this.analysisDao = new AnalysisDao(getEntityManager());
        }
        return analysisDao;
    }

    public AnalysisDao getAnalysisDao() {
        getAbstractDao();
        return analysisDao;
    }

    public List<Analysis> getAnalysisByFollow(FollowUp followUp) throws Exception {
        return getAnalysisDao().getAnalysisByFollow(followUp);
    }

    public List<Analysis> getAnalysisByPatient(Long patientid) throws Exception {
        return getAnalysisDao().getAnalysisByPatient(patientid);
    }

    public List<Analysis> getAnalysisByFollowAndStatus(FollowUp followUp, Laps lap) {
        return getAnalysisDao().getAnalysisByFollowAndStatus(followUp, lap);
    }

    public List<Analysis> getCurrentAnalysis(Laps lap) {
        List<Analysis> analysis = getAnalysisDao().getCurrentAnalysis(lap);
        analysis.sort((o1,o2) -> o2.getFollow().getDate().compareTo(o1.getFollow().getDate()));
        return analysis;
    }

    public void sendNotificationToPatient(Analysis analysis) {
        FollowUp followUp = followUpBuisness.find(analysis.getFollow().getFollowId());
        Object count = getAnalysisDao().sendNotificationToPatient(followUp.getVisitId().getPatientId());
        if ((Long)count == 0) {
            Utils.sendSms(followUp.getVisitId().getPatientId().getMobileNumber(), "Your analyses finished");
        }
    }

}
