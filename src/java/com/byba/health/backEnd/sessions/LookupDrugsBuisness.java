/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.LookupDrugsDao;
import com.byba.health.backEnd.entities.LookupDrugs;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class LookupDrugsBuisness extends AbstractBusiness<LookupDrugs> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private LookupDrugsDao lookupDrugsDao;

    public LookupDrugsBuisness() {
        super(LookupDrugs.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<LookupDrugs> getAbstractDao() {
        if (lookupDrugsDao == null) {
            this.lookupDrugsDao = new LookupDrugsDao(getEntityManager());
        }
        return lookupDrugsDao;
    }

    public LookupDrugsDao getAppointmentsDao() {
        getAbstractDao();
        return lookupDrugsDao;
    }

}
