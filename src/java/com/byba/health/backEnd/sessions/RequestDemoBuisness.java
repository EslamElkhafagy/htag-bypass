/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.backEnd.sessions;

import com.byba.health.backEnd.daos.AbstractDao;
import com.byba.health.backEnd.daos.RequestDemoDao;
import com.byba.health.backEnd.entities.RequestDemo;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author IbrahimShedid
 */
@Stateless
public class RequestDemoBuisness extends AbstractBusiness<RequestDemo> {
    @PersistenceContext(unitName = "byba-ssPU")
    private EntityManager em;
    private RequestDemoDao requestDemoDao;

    public RequestDemoBuisness() {
        super(RequestDemo.class);
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    protected AbstractDao<RequestDemo> getAbstractDao() {
        if (requestDemoDao == null) {
            this.requestDemoDao = new RequestDemoDao(getEntityManager());
        }
        return requestDemoDao;
    }

    public RequestDemoDao getRequestDemoDao() {
        getAbstractDao();
        return requestDemoDao;
    }

}
