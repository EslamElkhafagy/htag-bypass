package com.byba.health.middleWare.dto;

import com.byba.health.backEnd.entities.UserProfile;

/**
 * Created by Administrator on 2/26/2018.
 */
public class ResetPasswordDto {
    private String mobileNumber;
    private String code;
    private String password;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}