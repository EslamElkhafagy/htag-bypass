package com.byba.health.middleWare.dto;

import com.byba.health.backEnd.entities.UserProfile;

/**
 * Created by Administrator on 2/26/2018.
 */
public class RegisterDto {
    private UserProfile userProfile;
    private String fullName;
    private String government;
    private String clinicAddress;
    private String clinicGovernment;


    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGovernment() {
        return government;
    }

    public String getClinicAddress() {
        return clinicAddress;
    }

    public void setClinicAddress(String clinicAddress) {
        this.clinicAddress = clinicAddress;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getClinicGovernment() {
        return clinicGovernment;
    }

    public void setClinicGovernment(String clinicGovernment) {
        this.clinicGovernment = clinicGovernment;
    }
}
