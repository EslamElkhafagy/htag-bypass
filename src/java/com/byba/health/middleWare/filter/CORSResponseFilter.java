package com.byba.health.middleWare.filter;

import com.byba.health.backEnd.entities.Sessions;
import com.byba.health.backEnd.sessions.SessionsBuisness;

import javax.ejb.EJB;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CORSResponseFilter  implements ContainerResponseFilter, Filter{
    private FilterConfig filterConfig = null;
    // used for add headers on web services only by add com.byba.health.middleWare.filter to ApplicationConfig
//         String host = "http://htag.health/";
         String host1 = "www.htag.health/";
         String host2 = "htag.health/";
         String host3 = "159.122.144.82/";
         String host = "http:/127.0.0.1: 8080/";

    @EJB
    SessionsBuisness sessionsBuisness;

    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {

        MultivaluedMap<String, String> reqHeaders = requestContext.getHeaders();
//        reqHeaders.add("Access-Control-Allow-Origin", host );
        reqHeaders.add("Access-Control-Allow-Origin", "*" );
        reqHeaders.add("Access-Control-Allow-Credentials"," true");
        reqHeaders.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        reqHeaders.add("Access-Control-Allow-Headers", "*");

        MultivaluedMap<String, Object> headers = responseContext.getHeaders();

        headers.add("Access-Control-Allow-Origin", "*" );

        headers.add("Access-Control-Allow-Credentials"," true");
        headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        headers.add("Access-Control-Allow-Headers", "*");
//        headers.add("Access-Control-Expose-Headers", "X-XSRF-TOKEN,XSRF-TOKEN");

//        System.out.println("before while loop in crosresponsefiltter");
//        if(headers.containsKey("userSession")){
//            if(headers.get("userSession") != null){
//                List<Object> userSession = headers.get("userSession");
//                System.out.println("user session is found");
//
//            }
//
//        }


    }


//----------------------------------------------------------------------------------------------------------------------
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
        System.out.println("on init filter");
    }

    // used for add headers on whole responses by add com.byba.health.middleWare.filter in web.xml
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        if (filterConfig == null)
            return;

        response.addHeader("Access-Control-Allow-Origin", host);
        response.addHeader("Access-Control-Allow-Credentials"," true");
        response.addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
        response.addHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, X-Codingpedia,dteSendhdr");

/**
 *
 **/



        filterChain.doFilter(servletRequest,servletResponse);
    }

    @Override
    public void destroy() {
        this.filterConfig = null;
        System.out.println("on destroy filter");

    }


}