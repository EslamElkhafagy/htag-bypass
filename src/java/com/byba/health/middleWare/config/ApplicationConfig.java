/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.config;

import java.util.Set;
import javax.ws.rs.core.Application;

/**
 * @author IbrahimShedid
 */
@javax.ws.rs.ApplicationPath("bypass")
public class ApplicationConfig extends Application {

    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> resources = new java.util.HashSet<>();
        addRestResourceClasses(resources);
        resources.add(com.byba.health.middleWare.filter.CORSRequestFilter.class);
        resources.add(com.byba.health.middleWare.filter.CORSResponseFilter.class);

        return resources;
    }

    /**
     * Do not modify addRestResourceClasses() method. It is automatically
     * populated with all resources defined in the project. If required, comment
     * out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(com.byba.health.middleWare.service.RolesFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.UserloginFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.UserprofileFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.DoctorsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.AppointmentsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.AssistantFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.ClinicsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.SecretaryFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.PatientFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.AttachmentFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.QrCardFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.DrugsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.AnalysisFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.ExSheetFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.FollowUpFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.LookupAnalysisFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.LookupDrugsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.LookupRadiologyFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.RadiologyFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.VisitsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.PharmaciesFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.RadiationsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.LapsFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.CardsLookupFacadeREST.class);
        resources.add(com.byba.health.middleWare.service.RequestDemoFacadeREST.class);
    }

}
