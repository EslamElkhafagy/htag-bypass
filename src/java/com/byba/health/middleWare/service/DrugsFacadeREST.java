/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Drugs;
import com.byba.health.backEnd.sessions.DrugsBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("Drugs")
public class DrugsFacadeREST {

    @EJB
    DrugsBuisness drugsBuisness;

    public DrugsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Drugs entity) {
        drugsBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Drugs entity) {
        drugsBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        drugsBuisness.remove(drugsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Drugs find(@PathParam("id") Integer id) {
        return drugsBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Drugs> findAll() {
        return drugsBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Drugs> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return drugsBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(drugsBuisness.count());
    }

}
