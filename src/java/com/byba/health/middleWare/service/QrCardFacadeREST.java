/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Cardslookup;
import com.byba.health.backEnd.entities.Patient;
import com.byba.health.backEnd.entities.QrCards;
import com.byba.health.backEnd.sessions.QrCardsBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("card")
public class QrCardFacadeREST {

    @EJB
    QrCardsBuisness qrCardsBuisness;

    public QrCardFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public String create(QrCards entity) {
        return qrCardsBuisness.addCard(entity);
    }
    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, QrCards entity) {
        qrCardsBuisness.updateCard(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        qrCardsBuisness.remove(qrCardsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public QrCards find(@PathParam("id") Integer id) {
        return qrCardsBuisness.find(id);
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<QrCards> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return qrCardsBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(qrCardsBuisness.count());
    }



}
