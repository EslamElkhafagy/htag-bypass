/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;

import com.byba.health.backEnd.entities.Cardslookup;
import com.byba.health.backEnd.sessions.CardsLookupBuisness;
import com.google.gson.JsonObject;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("CardsLookup")
public class CardsLookupFacadeREST {

    @EJB
    CardsLookupBuisness cardsLookupBuisness;

    public CardsLookupFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Cardslookup entity) {
        cardsLookupBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Cardslookup entity) {
        cardsLookupBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        cardsLookupBuisness.remove(cardsLookupBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Cardslookup find(@PathParam("id") Integer id) {
        return cardsLookupBuisness.find(id);
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Cardslookup> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return cardsLookupBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(cardsLookupBuisness.count());
    }

    @GET
    @Path("checkNumber/{number}")
    @Produces("text/plain")
    public String checkNumber(@PathParam("number") String number) {
        Cardslookup cardslookup = cardsLookupBuisness.getCardByNumber(number);
        if (cardslookup == null)
            return "true";
        else
            return "false";
    }

    @GET
    @Path("checkPin/{pin}")
    @Produces("text/plain")
    public String checkPin(@PathParam("pin") String pin) {
        Cardslookup cardslookup = cardsLookupBuisness.getCardByPin(pin);
        if (cardslookup == null)
            return "true";
        else
            return "false";
    }

    @POST
    @Path("checkCardData")
    @Produces("text/plain")
    public String checkCard(Cardslookup cardslookup) {
        Cardslookup cardByNumber = cardsLookupBuisness.getCardByNumber(cardslookup.getCardnumber());

        if (cardByNumber == null)
            return "false";
        else {
            if (cardByNumber.isUsed())
                return "false";
            else
                return "true";
        }
    }


}
