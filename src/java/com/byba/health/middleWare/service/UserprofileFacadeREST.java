/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;

import com.byba.health.backEnd.EmailManagement.EmailService;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.backEnd.sessions.UserProfileBuisness;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;

import com.byba.health.middleWare.dto.RegisterDto;
import com.byba.health.middleWare.dto.ResetPasswordDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.apache.http.HttpHeaders;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

/**
 * @author IbrahimShedid
 */
@Path("userprofile")
public class UserprofileFacadeREST {

    @EJB
    private UserProfileBuisness userProfileBuisness;
    @EJB
    private EmailService emailService;

    public UserprofileFacadeREST() {
    }

    @POST
    @Consumes("application/json")
    public String create(RegisterDto registerClass) {
        try {
            userProfileBuisness.addNewwUser(registerClass);
            return "true";
        } catch (Exception e) {
            e.printStackTrace();
            return "false";

        }
    }

    @PUT
    @Consumes({"application/xml", "application/json"})
    public UserProfile edit(UserProfile entity) {
        return userProfileBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        userProfileBuisness.remove(userProfileBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public UserProfile find(@PathParam("id") Long id) {
        return userProfileBuisness.find(id);
    }


    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<UserProfile> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return userProfileBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(userProfileBuisness.count());
    }


    @POST
    @Path("login/")
    @Produces("application/json")
    public String getUserProfileByName(UserProfile userProfile) {
        String login = userProfileBuisness.Login(userProfile.getLoginName(), userProfile.getUserPassword());
        System.out.println("----------------------------"+login);
        return login;
    }
    @GET
    @Path("notAuthorised/")
    @Produces("application/json")
    public String notAuthorised() {

        return "notAuthorised";
    }

    @POST
    @Path("verifyEmailAccount/")
    @Produces("application/json")
    public void verifyEmailAccount() {
        try{
            emailService.sendEmail("aymanahmed88@gmail.com","subbb","mess");

        }catch (Exception e){
            System.out.println(e);
        }
    }

    @GET
    @Path("checkMail/{mail}")
    @Produces("text/plain")
    public String checkMail(@PathParam("mail") String mail) {
        UserProfile userByLoginName = userProfileBuisness.getUserByLoginName(mail);
        if (userByLoginName == null)
            return "true";
        else
            return "false";
    }

    @GET
    @Path("checkMobile/{mobile}")
    @Produces("text/plain")
    public String checkMobile(@PathParam("mobile") String mobileNumber) {
        UserProfile userByMobileNumber = userProfileBuisness.getUserByMobileNumber(mobileNumber);
        if (userByMobileNumber == null)
            return "true";
        else
            return "false";
    }

    @GET
    @Path("findAll")
    @Produces("application/json")
    public String findAll() {
        String jsonString = null;
        try {
            List<UserProfile> userprofile = userProfileBuisness.findAll();

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"userID", "firstName", "loginName", "defaultPassword", "userPassword"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(userprofile);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }
    @POST
    @Path("sendsms/{mobilenumber}")
    @Produces("application/json")
    public boolean sendSms(@PathParam("mobilenumber") String mobileNumber) {
        return userProfileBuisness.sendSms(mobileNumber);
    }

    @POST
    @Path("verifycode/{mobileNumber}/{code}")
    @Produces("application/json")
    public boolean verifyCode(@PathParam("mobileNumber") String mobileNumber, @PathParam("code")String code) {
        return userProfileBuisness.verifyCode(mobileNumber,code);
    }
    @POST
    @Path("sendpasswordcode/{mobileNumber}")
    @Produces("application/json")
    public boolean sendPasswordCode(@PathParam("mobileNumber") String mobileNumber) {
        return userProfileBuisness.sendSmsToResetPassword(mobileNumber);
    }
    @POST
    @Path("updatePassword")
    @Produces("application/json")
    public boolean updatePassword(ResetPasswordDto resetPasswordDto) {
         return userProfileBuisness.updatePassword(resetPasswordDto);
    }

}
