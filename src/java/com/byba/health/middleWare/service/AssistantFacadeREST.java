/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Assistant;
import com.byba.health.backEnd.sessions.AssistantBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("assistant")
public class AssistantFacadeREST {

    @EJB
    AssistantBuisness assistantBuisness;

    public AssistantFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Assistant entity) {
        assistantBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Assistant entity) {
        assistantBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        assistantBuisness.remove(assistantBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Assistant find(@PathParam("id") Integer id) {
        return assistantBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Assistant> findAll() {
        return assistantBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Assistant> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return assistantBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(assistantBuisness.count());
    }

}
