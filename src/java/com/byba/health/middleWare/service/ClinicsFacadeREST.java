/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Clinics;
import com.byba.health.backEnd.entities.UserProfile;
import com.byba.health.backEnd.sessions.ClinicsBuisness;
import com.byba.health.backEnd.sessions.UserProfileBuisness;
import com.byba.health.middleWare.dto.SearchDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("clinics")
public class ClinicsFacadeREST {

    @EJB
    ClinicsBuisness clinicsBuisness;
    @EJB
    UserProfileBuisness userProfileBuisness;

    public ClinicsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Clinics entity) {
        clinicsBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes("application/json")
    public void edit(@PathParam("id") Integer id, Clinics entity) {
        clinicsBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        clinicsBuisness.remove(clinicsBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Clinics find(@PathParam("id") Integer id) {
        return clinicsBuisness.find(id);
    }

    @GET
    @Path("doctorclinics/{name}")
    @Produces({"application/xml", "application/json"})
    public String findAllByDoctor(@PathParam("name") String name) {
        String jsonString = null;
        try {
            UserProfile userProfile = userProfileBuisness.getUserByLoginName(name);
            List<Clinics> clinics = clinicsBuisness.getClinicsByDoctor(userProfile);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"clinicId", "governmentLocation", "areaLocation", "address", "phone1", "phone2", "phone3", "mobile", "examinationFees"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(clinics);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Clinics> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return clinicsBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(clinicsBuisness.count());
    }

}
