/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.ExaminationSheet;
import com.byba.health.backEnd.sessions.ExSheetBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("ExaminationSheet")
public class ExSheetFacadeREST {

    @EJB
    ExSheetBuisness exSheetBuisness;

    public ExSheetFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(ExaminationSheet entity) {
        exSheetBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, ExaminationSheet entity) {
        exSheetBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        exSheetBuisness.remove(exSheetBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public ExaminationSheet find(@PathParam("id") Integer id) {
        return exSheetBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<ExaminationSheet> findAll() {
        return exSheetBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<ExaminationSheet> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return exSheetBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(exSheetBuisness.count());
    }

}
