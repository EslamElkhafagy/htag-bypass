/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Laps;
import com.byba.health.backEnd.sessions.LapsBuisness;
import com.byba.health.middleWare.dto.LapSearchDto;
import com.byba.health.middleWare.dto.SearchDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("laps")
public class LapsFacadeREST {

    @EJB
    LapsBuisness lapsBuisness;

    public LapsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Laps entity) {
        lapsBuisness.create(entity);
    }

    @PUT
    @Consumes({"application/xml", "application/json"})
    @Produces(MediaType.TEXT_PLAIN)
    public Laps edit(Laps entity) {
        try {
            return lapsBuisness.edit(entity);
        } catch (Exception ex) {
            return null;
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        lapsBuisness.remove(lapsBuisness.find(id));
    }

    @POST
    @Path("getLapsProfile/{loginName}")
    @Produces("application/json")
    public String getUserProfileByName(@PathParam("loginName") String loginName) {
        String jsonString = null;
        try {
            Laps laps = lapsBuisness.getLapsByEmail(loginName);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"lapId", "lapName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture",
                    "userProfile", "userId", "firstName", "defaultPassword", "userPassword", "loginName", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(laps);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @POST
    @Path("labSearch")
    @Produces("application/json")
    public String labSearch(SearchDto searchDto) {
        String jsonString = null;
        try {
            List<LapSearchDto> laps = lapsBuisness.getLapsSearchData(searchDto);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"lapId", "lapName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(laps);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }


    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Laps find(@PathParam("id") Integer id) {
        return lapsBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Laps> findAll() {
        return lapsBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Laps> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return lapsBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(lapsBuisness.count());
    }

}
