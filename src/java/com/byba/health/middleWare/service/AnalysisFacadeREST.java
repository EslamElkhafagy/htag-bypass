/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Analysis;
import com.byba.health.backEnd.sessions.AnalysisBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("Analysis")
public class AnalysisFacadeREST {

    @EJB
    AnalysisBuisness analysisBuisness;

    public AnalysisFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Analysis entity) {
        analysisBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Long id, Analysis analysis) {
        analysisBuisness.edit(analysis);
        analysisBuisness.sendNotificationToPatient(analysis);

    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        analysisBuisness.remove(analysisBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Analysis find(@PathParam("id") Long id) {
        return analysisBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Analysis> findAll() {
        return analysisBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Analysis> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return analysisBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(analysisBuisness.count());
    }

}
