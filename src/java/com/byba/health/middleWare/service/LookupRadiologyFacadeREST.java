/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.LookupRadiology;
import com.byba.health.backEnd.sessions.LookupRadiologyBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("LookupRadiology")
public class LookupRadiologyFacadeREST {

    @EJB
    LookupRadiologyBuisness lookupRadiologyBuisness;

    public LookupRadiologyFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(LookupRadiology entity) {
        lookupRadiologyBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, LookupRadiology entity) {
        lookupRadiologyBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        lookupRadiologyBuisness.remove(lookupRadiologyBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public LookupRadiology find(@PathParam("id") Integer id) {
        return lookupRadiologyBuisness.find(id);
    }

    @GET
    @Produces("application/json")
    public List<LookupRadiology> findAll() {
        return lookupRadiologyBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<LookupRadiology> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return lookupRadiologyBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(lookupRadiologyBuisness.count());
    }

}
