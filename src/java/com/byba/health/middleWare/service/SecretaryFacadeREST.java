/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Secretary;
import com.byba.health.backEnd.sessions.SecretaryBuisness;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.util.List;

/**
 * @author IbrahimShedid
 */
@Path("secretary")
public class SecretaryFacadeREST {

    @EJB
    SecretaryBuisness secretaryBuisness;

    public SecretaryFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Secretary entity) {
        secretaryBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Secretary entity) {
        secretaryBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        secretaryBuisness.remove(secretaryBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Secretary find(@PathParam("id") Integer id) {
        return secretaryBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Secretary> findAll() {
        return secretaryBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Secretary> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return secretaryBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(secretaryBuisness.count());
    }

}
