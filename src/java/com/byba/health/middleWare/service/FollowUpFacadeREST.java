/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Analysis;
import com.byba.health.backEnd.entities.FollowUp;
import com.byba.health.backEnd.entities.Radiology;
import com.byba.health.backEnd.entities.Visits;
import com.byba.health.backEnd.sessions.FollowUpBuisness;
import com.byba.health.middleWare.dto.LabSheetDto;
import com.byba.health.middleWare.dto.PatientTimelineDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("FollowUp")
public class FollowUpFacadeREST {

    @EJB
    FollowUpBuisness followUpBuisness;

    public FollowUpFacadeREST() {
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public void create(FollowUp entity) {
        followUpBuisness.addExaminationSheet(entity);
    }

    @POST
    @Path("saveExaminationSheetForLab")
    @Consumes("application/json")
    @Produces("application/json")
    public String createExaminationSheetForLab(LabSheetDto entity) {
        try {
            followUpBuisness.addExaminationSheetForLab(entity);
            return "true";
        } catch (Exception e) {
            return "false";
        }
    }

    @POST
    @Path("saveFollowUpTreatment")
    @Consumes("application/json")
    @Produces("application/json")
    public void saveFollowUpTreatment(FollowUp entity) {
        followUpBuisness.saveFollowUpTreatment(entity);
    }

    @POST
    @Path("saveFollowUpAnalysis")
    @Consumes("application/json")
    @Produces("application/json")
    public void saveFollowUpAnalysis(FollowUp entity) {
        followUpBuisness.saveFollowUpAnalysis(entity);
    }

    @POST
    @Path("saveFollowUpRadiologies")
    @Consumes("application/json")
    @Produces("application/json")
    public void saveFollowUpRadiologies(FollowUp entity) {
        followUpBuisness.saveFollowUpRadiologies(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, FollowUp entity) {
        followUpBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        followUpBuisness.remove(followUpBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public FollowUp find(@PathParam("id") Integer id) {
        return followUpBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<FollowUp> findAll() {
        return followUpBuisness.findAll();
    }

    @GET
    @Path("patientVisits/{patientId}")
    @Produces("application/json")
    public String getPatientVisits(@PathParam("patientId") Long patientId) {
        String jsonString = null;

        try {
            List<Visits> patientVisits = followUpBuisness.getPatientVisits(patientId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "date", "notes", "clinicId", "doctorId", "doctors", "doctorName"
                    , "followUpList", "followId", "date", "notes"
                    , "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "Direction", "pharId"
                    , "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"
                    , "analysisList", "id", "name", "notes", "result", "status",
                    "examinationSheetList", "id", "heartRate", "blood", "respRate", "tempreature", "objectiveHide", "assementHide", "notesHide",
                    "weight", "height", "subjective", "objective", "assement", "notes", "bmi", "bsa", "rythm", "bloodpresure2", "symptoms"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patientVisits);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("patientLatestVisit/{patientId}/{clinicId}")
    @Produces("application/json")
    public String getPatientLatestVisit(@PathParam("patientId") Long patientId, @PathParam("clinicId") Integer clinicId) {
        String jsonString = null;

        try {
            Visits patientLatestVisit = followUpBuisness.getPatientLatestVisit(patientId, clinicId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "date", "notes", "clinicId", "doctorId", "doctors", "doctorName"
                    , "followUpList", "followId", "date", "notes"
                    , "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "Direction", "pharId"
                    , "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"
                    , "analysisList", "id", "name", "notes", "result", "status",
                    "examinationSheetList", "id", "heartRate", "blood", "respRate", "tempreature", "objectiveHide", "assementHide", "notesHide",
                    "weight", "height", "subjective", "objective", "assement", "notes", "bmi", "bsa", "rythm", "bloodpresure2", "symptoms"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patientLatestVisit);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("patientLatestFollows/{patientId}/{clinicId}")
    @Produces("application/json")
    public String getPatientLatestFollows(@PathParam("patientId") Long patientId, @PathParam("clinicId") Integer clinicId) {
        String jsonString = null;

        try {
            List<FollowUp> patientLatestFollows = followUpBuisness.getPatientLatestFollows(patientId, clinicId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "id", "clinicId", "doctorId", "doctors", "doctorName",
                    "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "Direction", "pharId"
                    , "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"
                    , "analysisList", "id", "name", "notes", "result", "status",
                    "examinationSheetList", "id", "heartRate", "blood", "respRate", "tempreature", "objectiveHide", "assementHide", "notesHide",
                    "weight", "height", "subjective", "objective", "assement", "notes", "bmi", "bsa", "rythm", "bloodpresure2", "symptoms"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patientLatestFollows);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }
    /*
     * My Edit to get patient visit for all clinic and doctor
     * */
/*
    @GET
    @Path("patientLatestFollows/{patientId}/{clinicId}")
    @Produces("application/json")
    public String getAllPatientFollows(@PathParam("patientId") Long patientId) {
        String jsonString = null;

        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getAllFollowUpsByPatient(patientId);
            System.out.println("********    Patient     *************** \n "+followUpsByPatient.toString());
            System.out.println("********************** End Patient ******************");
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName",
                    "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "direction", "pharId"
                    , "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"
                    , "analysisList", "id", "name", "notes", "result", "status",
                    "examinationSheetList", "id", "heartRate", "blood", "respRate", "tempreature",
                    "weight", "height", "subjective", "objective", "assement", "notes", "bmi", "bsa", "rythm", "bloodpresure2", "symptoms", "objectiveHide", "assementHide", "notesHide"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    */
    @GET
    @Path("patientFollows/{patientId}/{clinicId}")
    @Produces("application/json")
    public String getPatientFollows(@PathParam("patientId") Long patientId, @PathParam("clinicId") Integer clinicId) {
        String jsonString = null;

        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getFollowUpsByPatient(patientId, clinicId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName",
                    "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "direction", "pharId"
                    , "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"
                    , "analysisList", "id", "name", "notes", "result", "status",
                    "examinationSheetList", "id", "heartRate", "blood", "respRate", "tempreature",
                    "weight", "height", "subjective", "objective", "assement", "notes", "bmi", "bsa", "rythm", "bloodpresure2", "symptoms", "objectiveHide", "assementHide", "notesHide"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("getAttachment/{id}/{number}")
    @Produces("application/json")
    public String getAttachment(@PathParam("id") Long id, @PathParam("number") Long number) {

        String jsonString = null;
        try {
            Object attachment = followUpBuisness.getAttachment(id, number);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "attachment"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(attachment);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;


    }

    @GET
    @Path("patientFollowsDrugs/{patientId}")
    @Produces("application/json")
    public String getPatientDrgus(@PathParam("patientId") Long patientId) {
        String jsonString = null;
        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getFollowUpsByPatient(patientId, 1);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "direction", "pharId"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("patientFollowsRadiology/{patientId}")
    @Produces("application/json")
    public String getPatientFollowsRadiology(@PathParam("patientId") Long patientId) {
        String jsonString = null;
        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getFollowUpsByPatient(patientId, 2);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status", "attachment"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("PatientRadiologies/{patientId}/{radId}")
    @Produces("application/json")
    public String getPatientRadiologies(@PathParam("patientId") Long patientId, @PathParam("radId") Long radId) {

        String jsonString = null;
        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getRadFollowUpsByStatus(patientId, radId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("currentRadiologies/{radId}")
    @Produces("application/json")
    public String getCurrentRadiologies(@PathParam("radId") Long radId) {

        List<Radiology> currentRadiologies = followUpBuisness.getCurrentRadiologies(radId);
        String jsonString = null;
        try {

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "name", "type", "bodyPart", "direction", "notes", "result", "status", "follow", "followId", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "patientId", "patientName"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(currentRadiologies);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jsonString;
    }

    @GET
    @Path("patientFollowsAnalysis/{patientId}")
    @Produces("application/json")
    public String getPatientFollowsAnalysis(@PathParam("patientId") Long patientId) {
        String jsonString = null;
        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getFollowUpsByPatient(patientId, 3);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "analysisList", "id", "name", "notes", "result", "attachment"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("openAnalysis/{id}")
    @Produces("application/json")
    public String getAnalysisById(@PathParam("id") Long id) {
        String jsonString = null;
        try {
            Analysis analysisById = followUpBuisness.getAnalysisById(id);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "name", "notes", "result", "attachment"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(analysisById);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("openRadiology/{id}")
    @Produces("application/json")
    public String getRadiologyById(@PathParam("id") Long id) {
        String jsonString = null;
        try {
            Radiology radiology = followUpBuisness.getRadiologyById(id);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "name", "type", "bodyPart", "direction", "notes", "result", "status"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(radiology);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("openDrugs/{id}")
    @Produces("application/json")
    public String getDrugsById(@PathParam("id") Long id) {
        String jsonString = null;
        try {
            FollowUp followById = followUpBuisness.getFollowById(id);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "drugsList", "id", "name", "dose", "duration", "notes", "result", "frequency", "direction", "pharId"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followById);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("PatientAnalysis/{patientId}/{lapId}")
    @Produces("application/json")
    public String getPatientAnalysisRadiology(@PathParam("patientId") Long patientId, @PathParam("lapId") Long lapId) {

        String jsonString = null;
        try {
            List<FollowUp> followUpsByPatient = followUpBuisness.getAnalysisFollowUpsByStatus(patientId, lapId);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty"
                    , "analysisList", "id", "name", "notes", "result", "status"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }
    @GET
    @Path("getAllPatientAnalysis/{patientId}")
    @Produces("application/json")
    public String getPatientAnalysisRadiology(@PathParam("patientId") Integer patientId) {

        String jsonString = null;
        try {
            List<Analysis> followUpsByPatient = followUpBuisness.getAnalysisFollowUpsByPatient(Long.valueOf(patientId));

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "name", "type", "bodyPart", "direction", "notes", "result", "status", "follow", "followId", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "patientId", "patientName"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }
    @GET
    @Path("getAllPatientRadiologies/{patientId}")
    @Produces("application/json")
    public String getAllPatientRadiologies(@PathParam("patientId") Integer patientId) {

        String jsonString = null;
        try {
            List<Radiology> followUpsByPatient = followUpBuisness.getAllPatientRadiologies(Long.valueOf(patientId));

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"followId", "date", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "radiologyList", "id", "name", "type", "bodyPart", "direction", "notes", "result", "status"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(followUpsByPatient);
        } catch (Exception ex) {
            return null;
        }
        return jsonString;
    }

    @GET
    @Path("currentAnalysis/{lapId}")
    @Produces("application/json")
    public String getCurrentAnalysis(@PathParam("lapId") Long lapId) {
        List<Analysis> currentAnalysis = followUpBuisness.getCurrentAnalysis(lapId);
        String jsonString = null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"id", "name", "type", "bodyPart", "direction", "notes", "result", "status", "follow", "followId", "visitId", "clinicId", "doctorId", "doctors", "doctorName", "mobileNumber", "specialty", "patientId", "patientName"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(currentAnalysis);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return jsonString;
    }

    @GET
    @Path("patientTimeline/{patientId}")
    @Produces("application/json")
    public String getPatientTimeline(@PathParam("patientId") Long patientId) {
        String jsonString = null;
        try {
            List<FollowUp> drugs = followUpBuisness.getFollowUpsByPatient(patientId, 1);
            List<FollowUp> analysis = followUpBuisness.getFollowUpsByPatient(patientId, 3);
            List<FollowUp> radiologies = followUpBuisness.getFollowUpsByPatient(patientId, 2);
            List<PatientTimelineDto> patientTimelineDtoList = new ArrayList<>();
            int rowNumber = 0;
            if (drugs != null)
                for (FollowUp f : drugs) {
                    if(f.getDrugsList().size()>0){
                        PatientTimelineDto patientTimelineDto = new PatientTimelineDto();
                        if (f.getVisitId().getClinicId().getDoctorId().getDoctors() != null &&
                                f.getVisitId().getClinicId().getDoctorId().getDoctors().getDoctorName() != null) {
                            patientTimelineDto.setDoctorName(f.getVisitId().getClinicId().getDoctorId().getDoctors().getDoctorName());
                            patientTimelineDto.setSpeciality(f.getVisitId().getClinicId().getDoctorId().getDoctors().getSpecialty());
                        }
                        patientTimelineDto.setOperationName("Prescription");
                        patientTimelineDto.setType("Pharmacy");
                        patientTimelineDto.setDate(f.getDate());
                        patientTimelineDto.setStatus("");
                        patientTimelineDto.setId(f.getFollowId());
                        patientTimelineDto.setRowNumber(++rowNumber);
                        patientTimelineDtoList.add(patientTimelineDto);

                    }}
            if (analysis != null)
                for (FollowUp f : analysis) {
                    for (Analysis a : f.getAnalysisList()) {
                        PatientTimelineDto patientTimelineDto = new PatientTimelineDto();
                        if (f.getVisitId().getClinicId().getDoctorId().getLaps() != null &&
                                f.getVisitId().getClinicId().getDoctorId().getLaps().getLapName() != null) {
                            patientTimelineDto.setDoctorName(f.getVisitId().getClinicId().getDoctorId().getLaps().getLapName());
                            patientTimelineDto.setSpeciality(null);
                        }
                        patientTimelineDto.setOperationName(a.getName());
                        patientTimelineDto.setType("lab");
                        patientTimelineDto.setDate(f.getDate());
                        patientTimelineDto.setStatus(a.getStatus());
                        patientTimelineDto.setId(a.getId());
                        patientTimelineDto.setRowNumber(++rowNumber);
                        patientTimelineDto.setNotes(a.getNotes());
                        patientTimelineDto.setHasAttachment(a.getAttachment() != null);
                        patientTimelineDtoList.add(patientTimelineDto);


                    }
                }
            if (analysis != null)
                for (FollowUp f : radiologies) {
                    for (Radiology r : f.getRadiologyList()) {
                        PatientTimelineDto patientTimelineDto = new PatientTimelineDto();
                        if (f.getVisitId().getClinicId().getDoctorId().getDoctors() != null &&
                                f.getVisitId().getClinicId().getDoctorId().getDoctors().getDoctorName() != null) {
                            patientTimelineDto.setDoctorName(f.getVisitId().getClinicId().getDoctorId().getDoctors().getDoctorName());
                            patientTimelineDto.setSpeciality(f.getVisitId().getClinicId().getDoctorId().getDoctors().getSpecialty());
                        }
                        patientTimelineDto.setOperationName(r.getName());
                        patientTimelineDto.setType("Radiation Center");
                        patientTimelineDto.setDate(f.getDate());
                        patientTimelineDto.setStatus(r.getStatus());
                        patientTimelineDto.setId(r.getId());
                        patientTimelineDto.setNotes(r.getNotes());
                        patientTimelineDto.setRowNumber(++rowNumber);
                        patientTimelineDto.setHasAttachment(r.getAttachment() != null);
                        patientTimelineDtoList.add(patientTimelineDto);
                    }
                }

            patientTimelineDtoList.sort((o1,o2) -> o2.getDate().compareTo(o1.getDate()));
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"rowNumber", "id", "doctorName", "speciality","operationName", "type", "date", "status","notes"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(patientTimelineDtoList);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return jsonString;
    }


    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<FollowUp> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return followUpBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(followUpBuisness.count());
    }

    private String mapObject(Object object, String[] Fieldnames) {
        String jsonString = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
        FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
        ObjectWriter writer = mapper.writer(filters);
        try {
            jsonString = writer.writeValueAsString(object);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return jsonString;
    }

}
