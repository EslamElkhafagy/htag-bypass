/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.Pharmacies;
import com.byba.health.backEnd.sessions.PharmaciesBuisness;
import com.byba.health.middleWare.dto.PharSearchDto;
import com.byba.health.middleWare.dto.SearchDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("pharmacies")
public class PharmaciesFacadeREST {

    @EJB
    PharmaciesBuisness pharmaciesBuisness;

    public PharmaciesFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Pharmacies entity) {
        pharmaciesBuisness.create(entity);
    }

    @PUT
    @Consumes({"application/xml", "application/json"})
    @Produces(MediaType.TEXT_PLAIN)
    public Pharmacies edit(Pharmacies entity) {
        Pharmacies edit = pharmaciesBuisness.edit(entity);
        return edit;
    }

    @PUT
    @Path("pharmacies/{mail}")
    @Consumes({"application/xml", "application/json"})
    @Produces(MediaType.TEXT_PLAIN)
    public Pharmacies edit(@PathParam("mail") String mail, String pic) {
        Pharmacies pharmaciesByEmail = pharmaciesBuisness.getPharmaciesByEmail(mail);
        pharmaciesByEmail.setPicture(pic);
        try {
            return pharmaciesBuisness.edit(pharmaciesByEmail);
        } catch (Exception e) {
            return null;
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        pharmaciesBuisness.remove(pharmaciesBuisness.find(id));
    }

    @POST
    @Path("getPharmaciesProfile/{loginName}")
    @Produces("application/json")
    public String getUserProfileByName(@PathParam("loginName") String loginName) {
        String jsonString = null;
        try {
            Pharmacies pharmacies = pharmaciesBuisness.getPharmaciesByEmail(loginName);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"pharId", "pharName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture",
                    "userProfile", "userId", "firstName", "defaultPassword", "userPassword", "loginName", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(pharmacies);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }
    @POST
    @Path("pharSearch/")
    @Produces("application/json")
    public String getUserProfileByName(SearchDto searchDto) {
        String jsonString = null;
        try {
            List<PharSearchDto> pharmacies = pharmaciesBuisness.getPharsSearch(searchDto);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"pharId", "pharName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(pharmacies);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }


    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Pharmacies find(@PathParam("id") Integer id) {
        return pharmaciesBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<Pharmacies> findAll() {
        return pharmaciesBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Pharmacies> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return pharmaciesBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(pharmaciesBuisness.count());
    }

}
