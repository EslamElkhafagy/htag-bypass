/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;

import com.byba.health.backEnd.entities.Attachments;
import com.byba.health.backEnd.sessions.AttachmentBuisness;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("attachment")
public class AttachmentFacadeREST {

    @EJB
    AttachmentBuisness attachmentBuisness;

    public AttachmentFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(Attachments entity) {
        attachmentBuisness.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({"application/xml", "application/json"})
    public void edit(@PathParam("id") Integer id, Attachments entity) {
        attachmentBuisness.edit(entity);
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        attachmentBuisness.remove(attachmentBuisness.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public Attachments find(@PathParam("id") Integer id) {
        return attachmentBuisness.find(id);
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<Attachments> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return attachmentBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(attachmentBuisness.count());
    }

    @GET
    @Path("/pdf")
    @Produces("application/pdf")
    public javax.ws.rs.core.Response getPdf() throws Exception
    {
        File file = new File("\\uploadedFiles\\Ibrahimshedid.pdf");
        FileInputStream fileInputStream = new FileInputStream(file);
        javax.ws.rs.core.Response.ResponseBuilder responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);
        responseBuilder.type("application/pdf");
        responseBuilder.header("Content-Disposition", "filename=test.pdf");
        return responseBuilder.build();
    }


}
