/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byba.health.middleWare.service;


import com.byba.health.backEnd.entities.RadiationCenters;
import com.byba.health.backEnd.sessions.RadiationBuisness;
import com.byba.health.middleWare.dto.RadSearchDto;
import com.byba.health.middleWare.dto.SearchDto;
import com.byba.health.middleWare.filter.PropertyFilterMixIn;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.ObjectWriter;
import org.codehaus.jackson.map.ser.FilterProvider;
import org.codehaus.jackson.map.ser.impl.SimpleBeanPropertyFilter;
import org.codehaus.jackson.map.ser.impl.SimpleFilterProvider;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author IbrahimShedid
 */
@Path("Radiations")
public class RadiationsFacadeREST {

    @EJB
    RadiationBuisness radiationBuisness;

    public RadiationsFacadeREST() {
    }


    @POST
    @Consumes({"application/xml", "application/json"})
    public void create(RadiationCenters entity) {
        radiationBuisness.create(entity);
    }

    @PUT
    @Consumes({"application/xml", "application/json"})
    @Produces(MediaType.TEXT_PLAIN)
    public RadiationCenters edit(RadiationCenters entity) {
        try {
            return radiationBuisness.edit(entity);
        } catch (Exception ex) {
            return null;
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        radiationBuisness.remove(radiationBuisness.find(id));
    }

    @POST
    @Path("getRadiationsProfile/{loginName}")
    @Produces("application/json")
    public String getUserProfileByName(@PathParam("loginName") String loginName) {
        String jsonString = null;
        try {
            RadiationCenters radiationCenters = radiationBuisness.getRadiationsByEmail(loginName);

            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"radId", "radName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture",
                    "userProfile", "userId", "firstName", "defaultPassword", "userPassword", "loginName", "type"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(radiationCenters);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }

    @POST
    @Path("radSearch")
    @Produces("application/json")
    public String getUserProfileByName(SearchDto searchDto) {
        String jsonString = null;
        try {
            List<RadSearchDto> radiationCenters = radiationBuisness.getRadsSearchData(searchDto);
            ObjectMapper mapper = new ObjectMapper();
            mapper.getSerializationConfig().addMixInAnnotations(Object.class, PropertyFilterMixIn.class);
            String[] Fieldnames = {"radId", "radName", "email", "mobileNo", "addMobileNo", "government", "area", "address",
                    "nationalId", "license", "picture"};
            FilterProvider filters = new SimpleFilterProvider().addFilter("filter fileds by name", SimpleBeanPropertyFilter.filterOutAllExcept(Fieldnames));
            ObjectWriter writer = mapper.writer(filters);
            jsonString = writer.writeValueAsString(radiationCenters);
        } catch (Exception ex) {
            Logger.getLogger(UserprofileFacadeREST.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonString;
    }


    @GET
    @Path("{id}")
    @Produces({"application/xml", "application/json"})
    public RadiationCenters find(@PathParam("id") Integer id) {
        return radiationBuisness.find(id);
    }

    @GET
    @Produces({"application/xml", "application/json"})
    public List<RadiationCenters> findAll() {
        return radiationBuisness.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({"application/xml", "application/json"})
    public List<RadiationCenters> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return radiationBuisness.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces("text/plain")
    public String countREST() {
        return String.valueOf(radiationBuisness.count());
    }

}
