/*global $, alert, console,jQuery */

$(function () {
	"use strict";

	/* tab panels */
	$('.tab-panels .tabs li').on('click', function () {
		var $panel = $(this).closest('.tab-panels');
		$panel.find('.tabs li.active').removeClass('active');
		$(this).addClass('active');

		//figure out which panel to show
		var panelToShow = $(this).attr('rel');

		//hide current panel
		$panel.find('.panel.active').slideUp(300, showNextPanel);

		//show next panel
		function showNextPanel() {
			$(this).removeClass('active');

			$('#' + panelToShow).slideDown(300, function () {
				$(this).addClass('active');
			});
		}
	});


	// textarea for lab and radiation result
	$(".btn_save").hide();

	$(".btn_edit").click(function(){
		$(".btn_save").show();
		$(".btn_edit").hide();
	});

	$(".btn_save").click(function(){
		$(".btn_edit").show();
		$(".btn_save").hide();
	});
});

function chooseSelect(selectValue) {
    var mainSelector = selectValue.value;
    var theSelect = document.getElementById('theSelect');

    if (mainSelector === "Doctor") {
        theSelect.removeAttribute("disabled");
        theSelect.classList.remove('btn_disabled');
    } else {
        theSelect.setAttribute("disabled", "true");
        theSelect.classList.add('btn_disabled');
        if (mainSelector === "") {
            theSelect.removeAttribute("disabled");
            theSelect.classList.remove('btn_disabled');
        }
    }

}