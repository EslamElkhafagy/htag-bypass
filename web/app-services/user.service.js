﻿(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http', 'ConfigService','$cookies'];
    function UserService($http, ConfigService,$cookies) {
        var service = {};

        service.GetAll = GetAll;
        service.GetById = GetById;
        service.Login = Login;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.checkMail = checkMail;
        service.checkMobile = checkMobile;
        service.sendSMSToUser = sendSMSToUser;
        service.sendRestPasswordSms = sendRestPasswordSms;
        service.VerfyCode = VerfyCode;
        service.resetPassword = resetPassword;
        service.getSessionValue=getSessionValue();

        var url = ConfigService.addressUrl;



        return service;



        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
         return headers;
        }
        function GetAll() {
            return $http.get(url + 'userprofile/findAll',{ headers: getSessionValue() }).then(handleSuccess, handleError);
        }

        function GetById(id) {
            return $http.get(url + 'api/users/' + id,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by id'));
        }

        function Login(user) {

           var c = $http.post(url + 'userprofile/login/', user,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by username'));

            console.log(c);

            return c;
        }

        function checkMail(mail) {
            return $http.get(url + 'userprofile/checkMail/' + mail,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        function checkMobile(mobile) {
            return $http.get(url + 'userprofile/checkMobile/' + mobile,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error checkMobile'));
        }

        function Create(user) {
            return $http.post(url + 'userprofile/', user,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        function Update(user) {
            return $http.put(url + 'userprofile/', user,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(user) {
            return $http.delete(url + 'userprofile/' + user.userID,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error deleting user'));
        }

        function sendSMSToUser(mobileNumber) {
            return $http.post(url + 'userprofile/sendsms/'+mobileNumber,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        function VerfyCode(mobileNumber, code) {
            return $http.post(url + 'userprofile/verifycode/' + mobileNumber + '/' + code,{ headers: getSessionValue() }).then(handleSuccess, handleError);
        }

        function sendRestPasswordSms(mobileNumber) {
            return $http.post(url + 'userprofile/sendpasswordcode/'+ mobileNumber,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        function resetPassword(dto) {
            return $http.post(url + 'userprofile/updatePassword' , dto,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        // private functions

        function handleSuccess(res) {
// console.log('data at handel data function at class user.service.js');
            console.log(res.data);
            // console.log('data at handel data function');
            // console.log(res.data['session_value']);
            // console.log('....');

// save user session in cookies
            var cookieExp = new Date();
            cookieExp.setDate(cookieExp.getDate() + 7);
            $cookies.putObject('userSession', res.data['session_value'], {expires: cookieExp});
//
            return res.data;
        }

        function handleError(error) {
            console.log(error)
            // return
            return function () {
                return {success: false, message: error};
            };
        }


    }

})();
