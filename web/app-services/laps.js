(function () {
    'use strict';

    angular
        .module('app')
        .factory('LapsService', LapsService);

    LapsService.$inject = ['$http', 'ConfigService','$cookies'];
    function LapsService($http, ConfigService,$cookies) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.Update = Update;
        service.getCurrentPatientList = getCurrentPatientList;
        var url = ConfigService.addressUrl;
        service.getSessionValue=getSessionValue();


        return service;

        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }
        // var sessionValue=$cookies.getObject('userSession');
        // var headers = { 'sessionValue': 'sessionValue'};

        function GetByEmail(email) {
            return $http.post(url + 'laps/getLapsProfile/' + email,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting Laps Profile'));
        }

        function Update(laps) {
            return $http.put(url + 'laps/', laps,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        function getCurrentPatientList(/*successCallBak,*/ radId) {
            return $http.get(url + 'FollowUp/currentAnalysis/' + radId,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting analysis Profile'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return {success: false, message: error};
            };
        }


    }

})();
