(function () {
    'use strict';

    angular
        .module('app')
        .factory('DoctorService', DoctorService);

    DoctorService.$inject = ['$http','ConfigService','$cookies'];
    function DoctorService($http,ConfigService,$cookies) {
        var service = {};

        service.GetAll = GetAll;
        service.GetByEmail = GetByEmail;
        service.checkName = checkName;
        service.Create = Create;
        service.Update = Update;
        service.UpdateDoctor = UpdateDoctor;
        service.getDoctorData = getDoctorData;
        service.getDoctorPatients = getDoctorPatients;
        service.Delete = Delete;
        service.getSessionValue=getSessionValue();


        var url = ConfigService.addressUrl;

        service.doctors ={};
        service.currentDoctor = {};

        return service;
        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }

      /*  var sessionValue=$cookies.getObject('userSession');
        var headers = { 'sessionValue': sessionValue};*/
        function GetAll() {
            return $http.get(url+'doctors/',{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);

        }
        function getDoctorData(search) {
            return $http.post(url+'doctors/doctorSearch/',search,{ headers: getSessionValue()}).then(handleSuccess).catch(handleError);
        }

        function GetByEmail(email) {
            return $http.get(url+'doctors/getDoctorProfile/' + email,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by username'));
        }
        function getDoctorPatients(id) {
            return $http.get(url+'patient/doctorPatients/' + id,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by username'));
        }


        function Create(doctor) {
            return $http.post(url+'doctors/', doctor,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error creating patient'));
        }
        function checkName(name) {
            return $http.get(url+'doctors/checkName/'+name,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by username'));
        }

        function Update(user) {
            return $http.put(url+'userprofile/' + user.id, user,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error updating user'));
        }
        function UpdateDoctor(doctor) {
            return $http.put(url+'doctors/',doctor,{ headers: getSessionValue()}).then(handleSuccess).catch(handleError);
        }

        function Delete(user) {
            return $http.delete(url+'userprofile/' + user.userID,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error deleting user'));
      }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
