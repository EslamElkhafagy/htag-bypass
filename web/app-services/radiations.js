(function () {
    'use strict';

    angular
        .module('app')
        .factory('RadiationsService', RadiationsService);

    RadiationsService.$inject = ['$http','ConfigService','$cookies'];
    function RadiationsService($http,ConfigService,$cookies) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.getCurrentPatientList = getCurrentPatientList;
        service.Update = Update;
        service.radId = null;
        service.getSessionValue=getSessionValue();


        var url = ConfigService.addressUrl;


        return service;

        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }


     /*   var sessionValue=$cookies.getObject('userSession');
        var headers = { 'sessionValue': sessionValue};*/
        function GetByEmail(email) {
            return $http.post(url+'Radiations/getRadiationsProfile/' + email,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting Laps Profile'));
        }

        function getCurrentPatientList(successCallBak, radId) {
            return $http.get(url + 'FollowUp/currentRadiologies/' + radId,{ headers: getSessionValue() }).then(successCallBak, handleError('Error getting analysis Profile'));
        }
        function Update(radiations) {
            return $http.put(url+'Radiations/',radiations,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }


        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
