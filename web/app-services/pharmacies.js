(function () {
    'use strict';

    angular
        .module('app')
        .factory('PharmaciesService', PharmaciesService);

    PharmaciesService.$inject = ['$http','ConfigService','$cookies'];
    function PharmaciesService($http,ConfigService,$cookies) {
        var service = {};

        service.GetByEmail = GetByEmail;
        service.Update = Update;
        service.getSessionValue=getSessionValue();


        var url = ConfigService.addressUrl;


        return service;
      /*  var sessionValue=$cookies.getObject('userSession');
        var headers = { 'sessionValue': sessionValue};*/

        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }
        function GetByEmail(email) {
            return $http.post(url+'pharmacies/getPharmaciesProfile/' + email,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting Pharmacies Profile'));
        }
        function Update(pharmacy) {
            return $http.put(url+'pharmacies/',pharmacy,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }


        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
