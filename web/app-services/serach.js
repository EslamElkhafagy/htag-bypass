(function () {
    'use strict';

    angular
        .module('app')
        .factory('SearchService', SearchService);

    SearchService.$inject = ['$http','ConfigService','$cookies'];
    function SearchService($http,ConfigService,$cookies) {
        var service = {};

        service.getDoctorData = getDoctorData;
        service.getPharsData = getPharsData;
        service.getRadsData = getRadsData;
        service.getLapsData = getLapsData;
        service.addRequestDemo = addRequestDemo;
        service.getSessionValue=getSessionValue();


        var url = ConfigService.addressUrl;

        service.doctors ={};
        service.phars ={};
        service.laps ={};
        service.rads ={};
        service.type;

        return service;
      /*  var sessionValue=$cookies.getObject('userSession');
        var headers = { 'sessionValue': sessionValue};*/
        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }

        function addRequestDemo(requestDemoObject) {
            return $http.post(url+'requestDemo/',requestDemoObject,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }
        function getDoctorData(search) {
            return $http.post(url+'doctors/doctorSearch/',search,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }
        function getPharsData(search) {
            return $http.post(url+'pharmacies/pharSearch/',search,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }
        function getRadsData(search) {
            return $http.post(url+'Radiations/radSearch/',search,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }
        function getLapsData(search) {
            return $http.post(url+'laps/labSearch/',search,{ headers: getSessionValue() }).then(handleSuccess).catch(handleError);
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
