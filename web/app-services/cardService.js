(function () {
    'use strict';

    angular
        .module('app')
        .factory('cardService', cardService);

    cardService.$inject = ['$http','ConfigService','$cookies'];
    function cardService($http,ConfigService,$cookies) {
        var service = {};

        service.GetAll = GetAll;
        service.GetCardById = GetById;
        // service.GetByUsernames = GetByUsernames;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        var url = ConfigService.addressUrl;
        service.getSessionValue=getSessionValue();

        return service;

        // var sessionValue=$cookies.getObject('userSession');
        // var headers = { 'sessionValue': sessionValue};
        function getSessionValue(){
            var sessionValue=$cookies.getObject('userSession');
            var headers = { 'sessionValue': sessionValue};
            return headers;
        }
        function GetAll() {
            return $http.get(url+'patient/findAll',{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting all users'));
        }

        function GetById(userId) {
            return $http.post(url+'patient/getPatientProfile/' + userId,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error getting user by username'));
        }


        // function GetByUsernames(username) {
        //     return $http.post(url+'userprofile/getUserProfile/' + username).then(handleSuccess, handleError('Error getting user by username'));
        // }

        function Create(card,patient) {
            return $http.post(url+'card/', card , card,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error creating patient'));
        }

        function Update(card) {
            return $http.put(url+'card/' + card.cardId, card,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error updating user'));
        }

        function Delete(user) {
            return $http.delete(url+'userprofile/' + user.userID,{ headers: getSessionValue() }).then(handleSuccess, handleError('Error deleting user'));
        }

        // private functions

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
