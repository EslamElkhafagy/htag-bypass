﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['ClinicService','DoctorService', 'UserService', '$rootScope', 'AuthenticationService', '$scope','$location'];

    function HomeController(ClinicService,DoctorService, UserService, $rootScope, AuthenticationService, $scope,$location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.doctor = {};
        vm.user = {};
        vm.clinic ={};
        vm.allUsers = [];
        vm.deleteUser = deleteUser;
        vm.logOut = logOut;
        vm.addClinic = addClinic;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile =false;
        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        initController();

        function initController() {
            loadCurrentDoctor();
            // $rootScope.loggedDoctor = vm.doctor;
            loadAllClinics();

        }
        function loadAllClinics() {
            ClinicService.GetAllByDoctor(vm.globaluserName)
                .then(function (clinics) {
                    vm.Clinics = clinics;
                });
        }
        function addClinic() {
            vm.dataClinicLoading = true;
            vm.clinic.doctorId = vm.doctor.userProfile.userId;

            ClinicService.Create(vm.clinic)
                .then(function (response) {

                    // if (response.success) {
                    swal(
                        'Congratlation!',
                        'Clinic Added Successful!',
                        'success'
                    );
                    $('#reopen_issue').modal('hide');
                    vm.dataClinicLoading = false;
                    // Reset the form model.
                    // Since Angular 1.3, set back to untouched state.
                    // $location.path('/patient');
                    // } else {
                    //     FlashService.Error(response.message);
                    //     asc.dataLoading = false;
                    // }

                });
        }
        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.doctor.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                };
                reader.readAsDataURL(input.files[0])
            }
        });
        function updateDoctor() {
            DoctorService.UpdateDoctor(vm.doctor)
                .then(function () {
                    $location.path('/doctor');

                });
        }

        $scope.uploadImg = function uploadImage() {
            updateDoctor();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            });
            $location.path('/doctor').replace();
        };
        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        };

        function logOut() {

            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            $cookies.remove('userSession');

            $location.path('/login');
        }

        function loadCurrentDoctor() {

            DoctorService.GetById(vm.globaluserName)
                .then(function (doctor) {
                    vm.doctor =doctor;
                    $scope.cropped.source =  vm.doctor.picture;
                    $('#img1').attr('src', vm.doctor.picture);
                    $('#img2').attr('src', vm.doctor.picture);
                    $('#img3').attr('src', vm.doctor.picture);
                    $('#circle').attr('src', vm.doctor.picture);
                    $('#imgcircle').attr('src', vm.doctor.picture);

                });
        }


        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function deleteUser(user) {
            UserService.Delete(user)
                .then(function () {
                    loadAllUsers();
                });
        }

    }

})();