﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('HeadController', HeadController);

    HeadController.$inject = ['DoctorService', '$rootScope', '$scope', '$location','ClinicService'];

    function HeadController(DoctorService, $rootScope, $scope, $location,ClinicService) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;

        vm.doctor = {};
        vm.user = {};
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;
        vm.clinicStatus = 0; // 0 have no clinics -- 1 have 1 clinic 2 have more than 1 clinic

        initController();

        function initController() {
            // if (DoctorService.currentDoctor.doctorName) {
            //     vm.doctor = DoctorService.currentDoctor;
            // } else {
                loadCurrentDoctors();
            // }

            var clinicId = localStorage.getItem("ClinicId");
            if (clinicId) {
                vm.clinicStatus = 1;
                ClinicService.flag = 1;
                vm.opendClinicId = clinicId;
            }
            vm.clinicFlag = ClinicService.flag; // 0 have no clinics -- 1 have 1 clinic 2 have more than 1 clinic

            vm.changeProfile = false;
        }

        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };



        function loadCurrentDoctors() {
            console.log("DoctorService.doctor",DoctorService.doctor);
            DoctorService.GetByEmail(vm.globaluserName)
                .then(function (doctor) {
                    vm.doctor = doctor;
                    DoctorService.currentDoctor = doctor;
                    $scope.cropped.source = vm.doctor.picture;
                    $('#img1').attr('src', vm.doctor.picture);
                    $('#img2').attr('src', vm.doctor.picture);
                    $('#img3').attr('src', vm.doctor.picture);
                    $('#circle').attr('src', vm.doctor.picture);
                    $('#imgcircle').attr('src', vm.doctor.picture);

                });
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.doctor.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateDoctor();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function() {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        };
        function updateDoctor() {
            DoctorService.UpdateDoctor(vm.doctor)
                .then(function () {
                    $location.path('/doctor');

                });
        }

    }

})();