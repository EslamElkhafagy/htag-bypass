﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('DoctorController', DoctorController);

    DoctorController.$inject = ['$cookieStore', 'ValueService', 'ClinicService', 'DoctorService', 'UserService', 'PatientService', '$rootScope', 'AuthenticationService', '$scope', '$location'];

    function DoctorController($cookieStore, ValueService, ClinicService, DoctorService, UserService, PatientService, $rootScope, AuthenticationService, $scope, $location) {

        $("#ar1").remove();
        $("#ar2").remove();
        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');


        vm.doctor = {};
        vm.user = {};
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.samePass = false;
        vm.searchPatient = {};
        vm.searchPatient.userProfile = {};
        vm.patient = {};
        vm.patient.userProfile = {};
        vm.query = {};
        vm.allPatients = [];
        vm.doctorPatients = [];
        vm.deletePatient = deletePatient;
        // vm.GetPatientByCardNumber = GetPatientByCardNumber;
        vm.addPatient = addPatient;
        vm.delClinic = delClinic;
        vm.viewExamination = viewExamination;
        vm.addClinic = addClinic;
        vm.clinicOp = clinicOp;
        vm.beforeEditClinic = beforeEditClinic;
        vm.logOut = logOut;
        vm.saveChanges = saveChanges;
        vm.savePasswordChanges = savePasswordChanges;
        vm.choose = choose;
        vm.chooseValueChanged = chooseValueChanged;
        vm.chooseclinic = chooseclinic;
        vm.allClinics = [];
        vm.clinic = {};
        vm.opendClinicId;
        vm.clinic.governmentLocation = "القاهرة";
        vm.dataPatientLoading = false;
        vm.clinicStatus = 0; // 0 have no clinics -- 1 have 1 clinic 2 have more than 1 clinic
        vm.clinicFlag = ClinicService.flag; // 0 have no clinics -- 1 have 1 clinic 2 have more than 1 clinic
        vm.search = true;
        vm.changeProfile = false;
        initController();

        function initController() {
            loadCurrentDoctors();
            var clinicId = localStorage.getItem("ClinicId")
            if (!vm.allClinics.length) {
                loadAllClinics();
            }

            vm.clinic.governmentLocation = 'المنوفية';
            vm.currentAddress = localStorage.getItem("address");
            if (clinicId) {
                vm.clinicStatus = 1;
                ClinicService.flag = 1;
                vm.opendClinicId = clinicId;
            }
            vm.changeProfile = false;
        }

        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.doctor.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateDoctor();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function () {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updateDoctor() {
            DoctorService.UpdateDoctor(vm.doctor)
                .then(function () {
                    $location.path('/doctor');

                });
        }

        vm.checkMail = function () {
            if (vm.patient.email) {
                UserService.checkMail(vm.patient.email)
                    .then(function (response) {
                        if (response == 'true')
                            vm.emailAviable = true;
                        else
                            vm.emailAviable = false;
                    })
            }


        };
        vm.checkMobile = function (mobileNo) {
            if (vm.patient.mobileNumber !== '') {
                UserService.checkMobile(vm.patient.mobileNumber)
                    .then(function (response) {
                        if (response == 'true')
                            vm.mobileAviable = true;
                        else
                            vm.mobileAviable = false;

                    })
            }
        };
        function GetPatientByCardNumber() {
            PatientService.GetPatientByCardNumber(vm.CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        vm.searchPatient = patient;
                        localStorage.setItem("PatientId", patient.patientId);
                        $location.url('/examinationSheet').replace();

                        vm.search = true;
                    } else {
                        vm.search = false;
                    }

                });
        }

        function viewExamination(patient) {
            if (patient) {
                //$cookieStore.put("ClinicId", vm.opendClinic.clinicId);
                localStorage.setItem("PatientId", patient.patientId);
                $location.url('/examinationSheet').replace();
            }
        }

        function chooseValueChanged(ClinicId) {
            // console.log('in chooseValueChanged');
            if (vm.opendClinicId) {
                localStorage.setItem("ClinicId", vm.opendClinicId);
                vm.clinicStatus = 1;
                ClinicService.flag = 1;
                location.reload();
            }

        }

        function chooseclinic(ClinicId) {

            if (ClinicId) {
                vm.opendClinicId = ClinicId
                localStorage.setItem("ClinicId", ClinicId);
                vm.clinicStatus = 1;
                ClinicService.flag = 1;
                location.reload();
            }

        }

        function choose(cli) {
            vm.opendClinicId = cli.clinicId;
            if (vm.opendClinicId) {
                localStorage.setItem("ClinicId", vm.opendClinicId);
                localStorage.setItem("address", cli.address);
                vm.clinicStatus = 1;
                ClinicService.flag = 1;
                window.location.href = "#!/doctor/patients";
                // window.location.href = "/byba_ss_war_exploded/#!/doctor/patientLog";
                //
                // location.reload();
            }

        }

        function delClinic(clId) {
            ClinicService.Delete(clId)
                .then(function (response) {
                    swal(
                        'Congratlation!',
                        'Clinic Deleted Successfully!',
                        'success'
                    ).then(function () {
                        window.location.reload();
                    });
                });

        }

        function beforeEditClinic(cl) {
            vm.clinic = cl;
            $('#add_clinic').modal('show');

        }

        function clinicOp() {
            if (vm.clinic.clinicId != null)
                editClinic();
            else
                addClinic();
        }

        function addClinic() {
            vm.dataClinicLoading = true;
            vm.clinic.doctorId = vm.doctor.userProfile.userId;

            ClinicService.Create(vm.clinic)
                .then(function (response) {

                    // if (response.success) {
                    swal(
                        'Congratlation!',
                        'Clinic Added Successfuly!',
                        'success'
                    ).then(function () {
                        window.location.reload();
                        //
                    });


                });
        }

        function editClinic() {
            vm.dataClinicLoading = true;
            vm.clinic.doctorId = vm.doctor.userProfile.userId;

            ClinicService.Edit(vm.clinic)
                .then(function (response) {
                    swal(
                        'Congratlation!',
                        'Clinic Saved Successfully!',
                        'success'
                    ).then(function () {
                        window.location.reload();
                    });
                });
        }

        function loadAllPatients() {
            PatientService.GetAll()
                .then(function (patients) {
                    vm.allPatients = patients;

                });
        }

        function loadDoctorPatients() {
            DoctorService.getDoctorPatients(vm.doctor.doctorId)
                .then(function (patients) {
                    vm.doctorPatients = patients;

                });
        }

        function addPatient() {
            vm.dataPatientLoading = true;
            vm.card = {};
            vm.patient.userProfile = {};
            vm.card.cardNumber = vm.patient.cardId.cardNumber;
            vm.card.cardPin = "";
            vm.patient.userProfile.loginName = vm.patient.mobileNumber;
            vm.patient.cardId = vm.card
            PatientService.addPatient(vm.patient)
                .then(function (response) {
                    if (response == '0') {
                        swal(
                            'Error!',
                            'Not a valid Card Data!',
                            'error'
                        );
                    }
                    else if (response == '2') {
                        swal(
                            'Error!',
                            'Error in Adding new patient!',
                            'error'
                        );
                    } else {
                        vm.patient = null;
                        $('#myModal3').modal('hide');
                        swal(
                            'Congratulation!',
                            'patient Created Successfully!',
                            'success'
                        ).then(function () {
                            localStorage.setItem("PatientId", response.patientId);
                            window.location.href = "/#!/examinationSheet";
                            // window.location.href = "/byba_ss_war_exploded/#!/examinationSheet";
                        });
                        vm.dataPatientLoading = false;
                    }
                });
        }


        function deletePatient(patient) {
            PatientService.Delete(patient)
                .then(function () {
                    loadAllPatients();
                });
        }

        vm.confirmPassword = function () {
            vm.samePass = vm.newPassword === vm.confirmPass ? true : false;
        };


        function logOut() {
            localStorage.clear();
            ClinicService.flag = 0;
            ClinicService.ClinicId = 0;
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();

        }

        function loadCurrentDoctors() {

            if (vm.globaluserName) {
                DoctorService.GetByEmail(vm.globaluserName)
                    .then(function (doctor) {
                        vm.doctor = doctor;
                        localStorage.setItem("doctorId", vm.doctor.doctorId);
                        $scope.cropped.source = vm.doctor.picture;
                        DoctorService.currentDoctor = doctor;
                        $('#img1').attr('src', vm.doctor.picture);
                        $('#img2').attr('src', vm.doctor.picture);
                        $('#img3').attr('src', vm.doctor.picture);
                        $('#circle').attr('src', vm.doctor.picture);
                        $('#imgcircle').attr('src', vm.doctor.picture);
                        loadDoctorPatients();


                    });
            } else {
                vm.logOut();
            }


        }


        function saveChanges() {
            vm.dataLoading = true;
            DoctorService.UpdateDoctor(vm.doctor)
                .then(function (response) {
                    if (response !== null) {
                        swal(
                            'Congratlation!',
                            'Changes Saved Successfuly!',
                            'success'
                        );
                        vm.dataLoading = false;
                        $location.path('/doctor');

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        vm.dataLoading = false;

                    }
                });
        }

        function savePasswordChanges() {
            vm.dataLoading2 = true;
            if (vm.oldPassword === vm.doctor.userProfile.userPassword) {
                vm.doctor.userProfile.userPassword = vm.newPassword;
                UserService.Update(vm.doctor.userProfile)
                    .then(function (response) {
                        if (response !== null) {
                            swal(
                                'Congratlation!',
                                'Changes Saved Successfuly!',
                                'success'
                            );
                            vm.dataLoading2 = false;
                            $location.path('/doctor');
                            // vm.oldPassword =null;
                            // vm.newPassword =null;
                            // vm.confirmPass =null;
                        } else {
                            swal(
                                'Error!',
                                'Changes not Saved!',
                                'error'
                            );
                            vm.dataLoading2 = false;

                        }
                    });

            } else {

                swal(
                    'Error!',
                    'Old password is incorrect',
                    'error'
                );
                vm.dataLoading2 = false;
            }
        }

        function loadAllClinics() {
            ClinicService.GetAllByDoctor($rootScope.globals.currentUser.email)
                .then(function (clinics) {
                    vm.allClinics = clinics;
                    if (vm.allClinics && vm.allClinics.length > 0 && ! localStorage.getItem("ClinicId")) {
                        vm.choose(vm.allClinics[0]);
                    }
                });
        }


        vm.specialities = [
            'Adolescent medicine',
            'Allergy and immunology',
            'Anaesthesiology',
            'Cardiology',
            'Cardiothoracic surgery',
            'Child and adolescent psychiatry and psychotherapy',
            'Clinical neurophysiology',
            'Colon and Rectal Surgery',
            'Dermatology-Venereology',
            'Emergency medicine',
            'Endocrinology',
            'Gastroenterology',
            'General practice',
            'General surgery',
            'Geriatrics',
            'Hospice and palliative medicine',
            'Infectious disease',
            'Internal medicine',
            'Interventional radiology',
            'Neonatology',
            'Nephrology',
            'Neurology',
            'Neuroradiology',
            'Neurosurgery',
            'Nuclear medicine',
            'Obstetrics and gynaecology',
            'Occupational medicine',
            'Ophthalmology',
            'Oral and maxillofacial surgery',
            'Orthodontics',
            'Orthopaedics',
            'Otorhinolaryngology',
            'Paediatric allergology',
            'Paediatric cardiology',
            'Paediatric endocrinology and diabetes',
            'Paediatric gastroenterology, hepatology and nutrition',
            'Paediatric haematology and oncology',
            'Paediatric infectious diseases',
            'Paediatric nephrology',
            'Paediatric respiratory medicine',
            'Paediatric rheumatology',
            'Paediatric surgery',
            'Paediatrics',
            'Pathology',
            'Physical medicine and rehabilitation',
            'Plastic, reconstructive and aesthetic surgery',
            'Psychiatry',
            'Public Health',
            'Pulmonology',
            'Radiation Oncology,Radiology,Sports medicine,Urology',
            'Vascular medicine,Vascular surgery'
        ];

        vm.subSpecialities = [
            'Paediatrics',
            'Paediatrics or Internal medicine',
            'None',
            'Internal medicine',
            'General surgery',
            'Psychiatry',
            'Neurology',
            'General Surgery',
            'None',
            'Anaesthetics',
            'Internal medicine',
            'Internal medicine',
            'None',
            'None',
            'Internal medicine or family medicine',
            'Various',
            'Pediatrics or Internal medicine',
            'None',
            'Radiology',
            'Paediatrics',
            'Internal medicine',
            'Internal medicine',
            'Radiology',
            'Surgery',
            'None',
            'None',
            'None',
            'None',
            'Surgery',
            'None',
            'General surgery',
            'None',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'Paediatrics',
            'General Surgery',
            'None',
            'None',
            'None',
            'General surgery',
            'Internal medicine',
            'None',
            'Internal medicine',
            'None',
            'None',
            'Family medicine',
            'General surgery',
            'Internal medicine',
            'General surgery'
        ];
    }

})();