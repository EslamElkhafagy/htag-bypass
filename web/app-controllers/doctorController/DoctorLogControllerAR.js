﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('DoctorLogControllerAR', DoctorLogControllerAR);

    DoctorLogControllerAR.$inject = ['DoctorService', 'UserService', '$rootScope', '$location','PatientService'];
    function DoctorLogControllerAR(DoctorService, UserService, $rootScope, $location,PatientService) {
        ﻿$('head').append('<link id="ar1" rel="stylesheet" href="dist/css/AdminLTE_AR.css">');
        $('head').append('<link id="ar2" rel="stylesheet" href="dist/css/bootstrap-ar.css">');

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.query = {};
        vm.doctor = {};
        vm.user = {};
        vm.allDoctors = [];
        vm.cardAssigndFlag = false;

        vm.viewDoctorProfile = viewDoctorProfile;

        initController();

        function initController() {
            loadAllDoctors();
        }
        function viewDoctorProfile(Doctor) {
            $rootScope.doctorVisit =  Doctor;
            loadCurrentPatient();

        }
        function loadCurrentPatient() {
            PatientService.GetPatientById(vm.globaluserName)
                .then(function (patient) {
                    vm.patient = patient;
                    if (vm.patient.cardId != null)
                        vm.cardAssigndFlag = vm.patient.cardId.isActive;
                    else
                        vm.cardAssigndFlag = false;
                    $scope.cropped.source = vm.patient.picture;
                    $('#img1').attr('src', vm.patient.picture);
                    $('#img2').attr('src', vm.patient.picture);
                    $('#img3').attr('src', vm.patient.picture);
                    $('#circle').attr('src', vm.patient.picture);
                    $('#imgcircle').attr('src', vm.patient.picture);


                });
        }

        function loadAllDoctors() {
            DoctorService.GetAll()
                .then(function (Doctors) {
                    vm.allDoctors = Doctors;
                });
        }

    }
})();