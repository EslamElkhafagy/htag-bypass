﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LapHeadController', LapHeadController);

    LapHeadController.$inject = ['LapsService', 'UserService','PatientService', '$rootScope', 'AuthenticationService', '$scope','$location'];

    function LapHeadController(LapsService, UserService,PatientService, $rootScope, AuthenticationService, $scope,$location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        vm.laps = null;
        vm.user = null;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;


        initController();

        function initController() {
            vm.changeProfile = false;
            loadCurrentLaps();
        }


        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        function loadCurrentLaps() {

            LapsService.GetByEmail(vm.globaluserName)
                .then(function (laps) {
                    vm.laps = laps;
                    $scope.cropped.source = vm.laps.picture;
                    $('#img1').attr('src', vm.laps.picture);
                    $('#img2').attr('src', vm.laps.picture);
                    $('#img3').attr('src', vm.laps.picture);
                    $('#circle').attr('src', vm.laps.picture);
                    $('#imgcircle').attr('src', vm.laps.picture);

                });
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.laps.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateLaps();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function() {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        };
        function updateLaps() {
            LapsService.Update(vm.laps)
                .then(function () {
                    $location.path('/laps');

                });
        }
    }

})();