﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PharHeadController', PharHeadController);

    PharHeadController.$inject = ['PharmaciesService', '$rootScope', '$scope', '$location'];

    function PharHeadController(PharmaciesService, $rootScope, $scope, $location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;

        vm.pharmacies = {};
        vm.user = {};
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;

        initController();

        function initController() {
            loadCurrentPharmacies();
            vm.changeProfile = false;

        }

        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };



        function loadCurrentPharmacies() {

            PharmaciesService.GetByEmail(vm.globaluserName)
                .then(function (pharmacies) {
                    vm.pharmacies = pharmacies;
                    $scope.cropped.source = vm.pharmacies.picture;
                    $('#img1').attr('src', vm.pharmacies.picture);
                    $('#img2').attr('src', vm.pharmacies.picture);
                    $('#img3').attr('src', vm.pharmacies.picture);
                    $('#circle').attr('src', vm.pharmacies.picture);
                    $('#imgcircle').attr('src', vm.pharmacies.picture);

                });
        }



        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.pharmacies.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                };
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updatePharmacy();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function() {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updatePharmacy(patient) {
            PharmaciesService.Update(vm.pharmacies)
                .then(function () {
                    $location.path('/pharmacies');

                });
        }

    }

})();