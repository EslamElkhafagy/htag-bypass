﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PharmaciesController', PharmaciesController);

    PharmaciesController.$inject = ['PharmaciesService', 'UserService', 'PatientService', '$rootScope', 'AuthenticationService', '$scope', '$location'];

    function PharmaciesController(PharmaciesService, UserService, PatientService, $rootScope, AuthenticationService, $scope, $location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.pharmacies = null;
        vm.user = null;
        vm.logOut = logOut;
        vm.saveChanges = saveChanges;
        vm.GetPatientByCardNumber = GetPatientByCardNumber;
        vm.savePasswordChanges = savePasswordChanges;
        vm.viewTreatments = viewLapSheet;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;
        vm.samePass = false;
        vm.patient = {};
        vm.search = false;


        initController();

        function initController() {
            vm.changeProfile = false;
            loadCurrentPharmacies();
        }
        function viewLapSheet(patient) {
            console.log(patient);
            if (patient) {
                //$cookieStore.put("ClinicId", vm.opendClinic.clinicId);
                localStorage.setItem("PatientId", patient.patientId);
                $location.url('/pharmacies/treatmentSheet').replace();
            }
        }

        function GetPatientByCardNumber() {
            PatientService.GetPatientByCardNumber(vm.CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        vm.patient = patient;
                        console.log(vm.patient);
                        vm.search = true;
                    } else {
                        vm.search = false;

                    }

                });
        }

        vm.confirmPassword = function () {
            vm.samePass = vm.newPassword === vm.confirmPass ? true : false;
        };
        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        function logOut() {
            localStorage.clear();
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            $location.url('/login').replace();
        }

        function loadCurrentPharmacies() {

            PharmaciesService.GetByEmail(vm.globaluserName)
                .then(function (pharmacies) {
                    vm.pharmacies = pharmacies;
                    localStorage.setItem("PharmacyId", vm.pharmacies.pharId);
                    $scope.cropped.source = vm.pharmacies.picture;
                    $('#img1').attr('src', vm.pharmacies.picture);
                    $('#img2').attr('src', vm.pharmacies.picture);
                    $('#img3').attr('src', vm.pharmacies.picture);
                    $('#circle').attr('src', vm.pharmacies.picture);
                    $('#imgcircle').attr('src', vm.pharmacies.picture);

                });
        }

        function saveChanges() {
            vm.dataLoading = true;
            PharmaciesService.Update(vm.pharmacies)
                .then(function (response) {
                    if (response !== null) {
                        swal(
                            'Congratlation!',
                            'Changes Saved Successfuly!',
                            'success'
                        );
                        vm.dataLoading = false;

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        vm.dataLoading = false;

                    }
                });
        }

        function savePasswordChanges() {
            vm.dataLoading2 = true;
            if (vm.oldPassword === vm.pharmacies.userProfile.userPassword) {
                vm.pharmacies.userProfile.userPassword = vm.newPassword;
                UserService.Update(vm.pharmacies.userProfile)
                    .then(function (response) {
                        if (response !== null) {
                            swal(
                                'Congratlation!',
                                'Changes Saved Successfuly!',
                                'success'
                            );
                            vm.dataLoading2 = false;

                        } else {
                            swal(
                                'Error!',
                                'Changes not Saved!',
                                'error'
                            );
                            vm.dataLoading2 = false;

                        }
                    });

            } else {

                swal(
                    'Error!',
                    'Old password is incorrect',
                    'error'
                );
                vm.dataLoading2 = false;
            }
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.pharmacies.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updatePharmacy();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            });
            $location.path('/pharmacies').replace();
        }

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updatePharmacy(patient) {
            PharmaciesService.Update(vm.pharmacies)
                .then(function () {
                    $location.path('/pharmacies');

                });
        }
    }

})();