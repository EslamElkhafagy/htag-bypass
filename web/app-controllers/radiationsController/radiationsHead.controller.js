﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RadiationsHeadController', RadiationsHeadController);

    RadiationsHeadController.$inject = ['RadiationsService', 'UserService', 'PatientService', '$rootScope', 'AuthenticationService', '$scope','$location'];

    function RadiationsHeadController(RadiationsService, UserService, PatientService, $rootScope, AuthenticationService, $scope,$location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;

        vm.radiations = null;
        vm.user = null;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;
        vm.changeProfile = false;


        initController();

        function initController() {
            vm.changeProfile = false;
            loadCurrentRadiations();
        }




        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };


        function loadCurrentRadiations() {

            RadiationsService.GetByEmail(vm.globaluserName)
                .then(function (radiations) {
                    vm.radiations = radiations;
                    $scope.cropped.source = vm.radiations.picture;
                    $('#img1').attr('src', vm.radiations.picture);
                    $('#img2').attr('src', vm.radiations.picture);
                    $('#img3').attr('src', vm.radiations.picture);
                    $('#circle').attr('src', vm.radiations.picture);
                    $('#imgcircle').attr('src', vm.radiations.picture);

                });
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.radiations.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateRadiation();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            }).then(function() {
                window.location.reload();
            });
        };

        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updateRadiation(patient) {
            RadiationsService.Update(vm.radiations)
                .then(function () {
                    $location.path('/radiation');

                });
        }
    }

})();