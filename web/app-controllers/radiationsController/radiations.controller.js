﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('RadiationsController', RadiationsController);

    RadiationsController.$inject = ['RadiationsService', 'UserService', 'PatientService', '$rootScope', 'AuthenticationService', '$scope', '$location', 'examinationSheet'];

    function RadiationsController(RadiationsService, UserService, PatientService, $rootScope, AuthenticationService, $scope, $location, examinationSheetService) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.radiations = null;
        vm.user = null;
        vm.logOut = logOut;
        vm.saveChanges = saveChanges;
        vm.GetPatientByCardNumber = GetPatientByCardNumber;
        vm.savePasswordChanges = savePasswordChanges;
        vm.viewRadSheet = viewRadSheet;
        vm.globaluserName = $rootScope.globals.currentUser.email;
        vm.changeProfile = false;
        vm.samePass = false;
        vm.changeProfile = false;
        vm.patient = {};
        vm.search = false;


        initController();

        function initController() {
            vm.changeProfile = false;
            vm.up = false;

            loadCurrentRadiations();
        }

        function GetPatientByCardNumber() {
            PatientService.GetPatientByCardNumber(vm.CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        vm.patient = patient;
                        console.log(vm.patient);
                        vm.search = true;
                    } else {
                        vm.search = false;

                    }

                });
        }

        function viewRadSheet(patient) {
            console.log(patient);
            if (patient) {
                //$cookieStore.put("ClinicId", vm.opendClinic.clinicId);
                localStorage.setItem("PatientId", patient.patientId);
                $location.url('/radiation/radiationSheet').replace();
            }
        }

        vm.confirmPassword = function () {
            vm.samePass = vm.newPassword === vm.confirmPass ? true : false;
        };
        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        function logOut() {
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            $location.url('/login').replace();
        }

        function loadCurrentRadiations() {

            RadiationsService.GetByEmail(vm.globaluserName)
                .then(function (radiations) {
                    vm.radiations = radiations;
                    localStorage.setItem("radId", vm.radiations.radId);
                    RadiationsService.radId = vm.radiations.radId;
                    $scope.cropped.source = vm.radiations.picture;
                    $('#img1').attr('src', vm.radiations.picture);
                    $('#img2').attr('src', vm.radiations.picture);
                    $('#img3').attr('src', vm.radiations.picture);
                    $('#circle').attr('src', vm.radiations.picture);
                    $('#imgcircle').attr('src', vm.radiations.picture);

                });
        }


        function saveChanges() {
            vm.dataLoading = true;
            RadiationsService.Update(vm.radiations)
                .then(function (response) {
                    if (response !== null) {
                        swal(
                            'Congratlation!',
                            'Changes Saved Successfuly!',
                            'success'
                        );
                        vm.dataLoading = false;

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        vm.dataLoading = false;

                    }
                });
        }


        function savePasswordChanges() {
            vm.dataLoading2 = true;
            if (vm.oldPassword === vm.radiations.userProfile.userPassword) {
                vm.radiations.userProfile.userPassword = vm.newPassword;
                UserService.Update(vm.radiations.userProfile)
                    .then(function (response) {
                        if (response !== null) {
                            swal(
                                'Congratlation!',
                                'Changes Saved Successfuly!',
                                'success'
                            );
                            vm.dataLoading2 = false;

                        } else {
                            swal(
                                'Error!',
                                'Changes not Saved!',
                                'error'
                            );
                            vm.dataLoading2 = false;

                        }
                    });

            } else {

                swal(
                    'Error!',
                    'Old password is incorrect',
                    'error'
                );
                vm.dataLoading2 = false;
            }
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.radiations.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updateRadiation();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Successfully!",
                icon: "success",
                button: "OK!",
            });
            $location.path('/radiation').replace();
        }
        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        }
        function updateRadiation(patient) {
            RadiationsService.Update(vm.radiations)
                .then(function () {
                    $location.path('/radiation');
                });
        }

        function getCurrentPatientList() {
            RadiationsService.getCurrentPatientList(getCurrentPatientListsuccess, localStorage.getItem("radId"));
        }

        function getCurrentPatientListsuccess(resp) {
            // _this.followUpRadio = resp.data
            vm.currentList = _.groupBy(resp.data, function (item) {
                if (item.follow.visitId.patientId) {
                    return item.follow.visitId.patientId.patientName
                }
            });
            console.log(vm.currentList);
        }

        getCurrentPatientList();
        vm.cancelTest = function (radio, patientIndex, followUpIndex) {
            examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'New', patientIndex, followUpIndex));
            // console.log(prePareRadiologyTosave(radio, 'New', patientIndex, followUpIndex));
        };

        vm.saveRadio = function (radio, patientIndex, followUpIndex) {
            $rootScope.showLoader = true;
            var f = document.getElementById('ra').files[0],
                reader = new FileReader();
            if (f) {
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        radio.attachment = e.target.result;
                        examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', patientIndex, followUpIndex));

                    });
                };
                reader.readAsDataURL(f);
            } else {
                examinationSheetService.saveRadio(saveFollowUpRadioSuccess, prePareRadiologyTosave(radio, 'Finished', patientIndex, followUpIndex));

            }
            // console.log(prePareRadiologyTosave(radio, 'Finished', patientIndex, followUpIndex));

        };
        function prePareRadiologyTosave(radio, status, patientIndex, followUpIndex) {
            radio.status = status;
            // radio.follow = {}
            // radio.follow.followId = vm.currentList[patientIndex][followUpIndex].follow.followId;
            radio.radioligyCenterId = {}
            radio.radioligyCenterId.radId = localStorage.getItem("radId");
            return radio;
        }

        function saveFollowUpRadioSuccess(resp) {
            $rootScope.showLoader = false;
            swal(
                'Congratlation!',
                'Examination Sheet and Treatment Plan Create Successfully',
                'success'
            ).then(function () {
                vm.loading = false;
                getCurrentPatientList();
            });
        }
    }
})();