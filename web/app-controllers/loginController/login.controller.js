﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'AuthenticationService', 'FlashService', '$cookies', '$rootScope'];

    function LoginController($location, AuthenticationService, FlashService, $cookies, $rootScope) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;

        vm.errorMessage = "";
        vm.login = login;
        vm.userId = null;
        (function initController() {
            // console.log($rootScope.remembers);
            if ($rootScope.remembers) {
                if ($rootScope.remembers.currentUser) {
                    vm.user={};
                    vm.username = $rootScope.remembers.currentUser.email;
                    vm.password = $rootScope.remembers.currentUser.password;
                    vm.user.loginName = vm.username;
                    vm.user.userPassword = vm.password;
                    login();
                }
            }
            localStorage.clear();
            // reset login status
        })();

        function login() {

            vm.dataLoading = true;
            vm.user = {};
            vm.user.loginName = vm.username;
            vm.user.userPassword = vm.password;
            AuthenticationService.Login(vm.user, function (response) {
                console.log('response data at login function in class login.controller');
                console.log(response.data);
                console.log('response data at login function in class login.controller');

                if (response.success) {
                    AuthenticationService.SetCredentials(vm.user.loginName, vm.user.userPassword, response.type, vm.remember);
                    localStorage.clear();
                    if (response.type === 'patient') {
                        $location.path('/ar/patient/patientHistory');
                    } else if (response.type === 'doctor') {
                        $location.path('/doctor');
                    } else if (response.type === 'pharmacies') {
                        $location.path('/pharmacies');
                    } else if (response.type === 'laps') {
                        $location.path('/laps/currentTest');
                    } else if (response.type === 'radiations') {
                        $location.path('/radiation');
                    } else {
                        FlashService.Error(response.message);
                        vm.dataLoading = false;
                        vm.errorMessage = "Invalid Login";
                    }

                } else {
                    FlashService.Error(response.message);
                    vm.errorMessage = "Invalid Login";

                    vm.dataLoading = false;
                }

            });
        };
    }

})();

function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}
