/**
 * Created by Administrator on 2/24/2018.
 */

(function () {
    'use strict';

    var app = angular.module('app')
        .controller('examinationSheetHeadController', examinationSheetHeadController);

    examinationSheetHeadController.$inject = ['PatientService','$rootScope'];
    function examinationSheetHeadController(PatientService,$rootScope) {
        $("#ar1").remove();
        $("#ar2").remove();

        var _this = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                return true;
        }
        if (!logged)
            $location.path('/login');
        //_________________________________________________________________________Patient Info

        loadCurrentPatient();

        function loadCurrentPatient() {
            PatientService.GetPatientByIds(localStorage.getItem("PatientId"))
                .then(function (patient) {
                    _this.patient = patient;
                    $('#imgx').attr('src', _this.patient.picture);

                });
        }


    };

})();