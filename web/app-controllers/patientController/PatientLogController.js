﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PatientLogController', PatientLogController);

    PatientLogController.$inject = ['PatientService', 'UserService', '$rootScope', '$location'];
    function PatientLogController(PatientService, UserService, $rootScope, $location) {
        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.query = {}
        vm.patient = {};
        vm.allPatients = [];
        vm.deletePatient = deletePatient;
        vm.addPatient = addPatient;
        vm.globalUserType = $rootScope.globals.currentUser.type;

        initController();

        function initController() {
            // loadAllPatients();
        }

        function loadAllPatients() {
            PatientService.GetAll()
                .then(function (patients) {
                    vm.allPatients = patients;

                });
        }

        function deletePatient(patient) {
            PatientService.Delete(patient)
                .then(function () {
                    loadAllPatients();
                });
        }

    }

    function addPatient() {
        PatientService.Create(vm.patient)
            .then(function (response) {
                vm.patient = null;
                $('#reopen_issue').modal('hide');
                swal(
                    'Congratlation!',
                    'patient Created Successfuly!',
                    'success'
                );
                $location.path('/patientlog')

            });
    }
})();