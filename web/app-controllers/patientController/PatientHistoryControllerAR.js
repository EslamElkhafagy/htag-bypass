﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PatientHistoryControllerAR', PatientHistoryControllerAR);

    PatientHistoryControllerAR.$inject = ['PatientService', 'UserService', '$rootScope', '$location','examinationSheet'];
    function PatientHistoryControllerAR(PatientService, UserService, $rootScope, $location,examinationSheetService) {
        ﻿$('head').append('<link id="ar1" rel="stylesheet" href="dist/css/AdminLTE_AR.css">');
        $('head').append('<link id="ar2" rel="stylesheet" href="dist/css/bootstrap-ar.css">');

        var vm = this;
        vm.patientTimeLine = [];
        loadCurrentPatient();

        function loadPatientTimeLine(patientId) {
            PatientService.getPatientTimeline(patientId).then(function (response) {
                vm.patientTimeLine = response;
            })
        }


        function loadCurrentPatient() {
            if (PatientService.patient && PatientService.patient.patientId) {
                loadPatientTimeLine(PatientService.patient.patientId);
            } else {
                var globaluserName = $rootScope.globals.currentUser.email;
                PatientService.GetPatientById(globaluserName)
                    .then(function (patient) {
                        vm.patient = patient;
                        loadPatientTimeLine(vm.patient.patientId);
                    });
            }
        }


        vm.getCurrentTimeLineDetails = function (timeLine) {
            vm.selectedTimeLine = timeLine;
            if (timeLine && timeLine.type) {
                resetAllSeletedTypes();
                if (timeLine.type.toLowerCase() === 'pharmacy') {
                    PatientService.getDrugListDetails(timeLine.id).then(function (response) {
                        vm.followUp = response;
                        vm.showDrugs = true;
                    })
                }
                else if (timeLine.type.toLowerCase() === 'lab') {
                    PatientService.getLabTestDetails(timeLine.id).then(function (response) {
                        vm.followLab = response;
                        vm.showLabTest = true;
                    })
                }
                else if (timeLine.type.toLowerCase() === 'radiation center') {
                    PatientService.getRadiationDetails(timeLine.id).then(function (response) {
                        vm.followRadio = response;
                        vm.radiotest = true;
                    })
                }
            }

        }
        function resetAllSeletedTypes() {
            vm.showDrugs = vm.showLabTest = vm.radiotest = false;
        }

        vm.openFile = function openFile(id, number) {
            examinationSheetService.getAttachment(id, number)
                .then(function (res) {
                    if (res) {
                        if (res.attachment) {
                            var content = res.attachment.split(',');
                            var contentType =  content[0].split(':')[1]
                            const byteCharacters = atob(content[1]);
                            var byteNumbers = new Array(byteCharacters.length);
                            for (var i = 0; i < byteCharacters.length; i++) {
                                byteNumbers[i] = byteCharacters.charCodeAt(i);
                            }
                            var byteArray = new Uint8Array(byteNumbers);
                            var filename = "attachment"
                            var linkElement = document.createElement('a');
                            try {
                                var blob = new Blob([byteArray], { type : contentType});
                                var url = window.URL.createObjectURL(blob);
                                linkElement.setAttribute('href', url);
                                linkElement.setAttribute("download", filename);
                                var clickEvent = new MouseEvent("click", {
                                    "view": window,
                                    "bubbles": true,
                                    "cancelable": false
                                });
                                linkElement.dispatchEvent(clickEvent);
                            } catch (ex) {
                                console.log(ex);
                            }
                        }
                    }
                });
        };
    }

})();
