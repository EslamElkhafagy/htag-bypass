﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PatientHomeControllerAR', PatientHomeControllerAR);
    PatientHomeControllerAR.$inject = ['DoctorService', 'ClinicService', '$location', 'cardService', 'PatientService', 'UserService', '$rootScope', '$scope', 'AuthenticationService'];
    function PatientHomeControllerAR(DoctorService, ClinicService, $location, cardService, PatientService, UserService, $rootScope, $scope, AuthenticationService) {

        ﻿$('head').append('<link id="ar1" rel="stylesheet" href="dist/css/AdminLTE_AR.css">');
        $('head').append('<link id="ar2" rel="stylesheet" href="dist/css/bootstrap-ar.css">');

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.globaluserName = $rootScope.globals.currentUser.email;

        vm.patient = null;
        vm.qrCards = null;
        vm.user = null;
        vm.allUsers = [];
        vm.allAttchFiles = [];
        vm.cardAssigndFlag = false;
        vm.loginFlag = false;
        vm.confirmPass = "";
        vm.samePass = false;
        vm.attachment = null;
        vm.attachmentValue = null;
        vm.changeProfile = false;
        vm.imgPrev = null;
        vm.docName = "";
        vm.docSize = "";
        vm.selectedDoctor = {};
        vm.query = {}
        vm.allDoctors = [];

        vm.assignCard = assignCard;
        vm.saveChanges = saveChanges;
        vm.savePasswordChanges = savePasswordChanges;
        vm.add = add;
        vm.preview = preview;
        vm.logOut = logOut;

        initController();


        vm.confirmPassword = function xc() {
            vm.samePass = vm.patient.password === vm.samePass ? true : false;
        };
        $scope.cropped = {
            source: 'https://raw.githubusercontent.com/Foliotek/Croppie/master/demo/demo-1.jpg'
        };

        vm.confirmPassword = function () {
            vm.samePass = vm.newPassword === vm.confirmPass ? true : false;
        };

        function initController() {
            vm.changeProfile = false;
            if (PatientService.patient) {
                vm.patient = PatientService.patient;
                checkCardAssignAndLoadImages();
                loadAllAttachFiles();
            } else {
                loadCurrentPatient();
            }
        }

        function loadCurrentPatient() {
            PatientService.GetPatientById(vm.globaluserName)
                .then(function (patient) {
                    vm.patient = patient;
                    PatientService.patient = patient;
                    checkCardAssignAndLoadImages();
                    loadAllAttachFiles();
                });
        }

        function loadAllAttachFiles() {
            if (PatientService.allAttchFiles && PatientService.patient && PatientService.patient.patientId == vm.patient.patientId)
                vm.allAttchFiles = PatientService.allAttchFiles;
            else {
                PatientService.getUploadFiles(vm.patient.patientId)
                    .then(function (allAttchFiles) {
                        vm.allAttchFiles = allAttchFiles;
                        PatientService.allAttchFiles = allAttchFiles;
                    });
            }
        }

        function checkCardAssignAndLoadImages() {
            if (vm.patient.cardId != null)
                vm.cardAssigndFlag = vm.patient.cardId.isActive;
            else
                vm.cardAssigndFlag = false;
            if($scope.cropped)
            $scope.cropped.source = vm.patient.picture;
            $('#img1').attr('src', vm.patient.picture);
            $('#img2').attr('src', vm.patient.picture);
            $('#img3').attr('src', vm.patient.picture);
            $('#circle').attr('src', vm.patient.picture);
            $('#imgcircle').attr('src', vm.patient.picture);
        }
        function saveChanges() {
            vm.dataLoading = true;
            PatientService.UpdatePatient(vm.patient)
                .then(function (response) {
                    if (response !== null) {
                        swal(
                            'Congratlation!',
                            'Changes Saved Successfuly!',
                            'success'
                        );
                        vm.dataLoading = false;

                    } else {
                        swal(
                            'Error!',
                            'Changes not Saved!',
                            'error'
                        );
                        vm.dataLoading = false;

                    }
                });
        }

        function savePasswordChanges() {
            vm.dataLoading2 = true;
            if (vm.oldPassword === vm.patient.userProfile.userPassword) {
                vm.patient.userProfile.userPassword = vm.newPassword;
                UserService.Update(vm.patient.userProfile)
                    .then(function (response) {
                        if (response !== null) {
                            swal(
                                'Congratlation!',
                                'Changes Saved Successfuly!',
                                'success'
                            );
                            vm.dataLoading2 = false;

                        } else {
                            swal(
                                'Error!',
                                'Changes not Saved!',
                                'error'
                            );
                            vm.dataLoading2 = false;

                        }
                    });

            } else {

                swal(
                    'Error!',
                    'Old password is incorrect',
                    'error'
                );
                vm.dataLoading2 = false;
            }
        }

        $('#upload').on('change', function () {
            var input = this;
            vm.base64 = input;
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {
                        $scope.cropped.source = e.target.result;
                        vm.patient.picture = e.target.result;
                        // var binaryData = Json.stringify(e.target.result);
                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $('#file').on('change', function () {
            var input = this;
            vm.base64 = input;

            if (input.files && input.files[0]) {
                var reader = new FileReader();
                vm.docName = input.files[0].name;
                vm.docSize = input.files[0].size;
                console.log(input.files[0].name);
                reader.onload = function (e) {
                    // bind new Image to Component
                    $scope.$apply(function () {

                        vm.attachmentValue = e.target.result;

                    });
                }
                reader.readAsDataURL(input.files[0])
            }
        });
        $scope.uploadImg = function uploadImage() {
            updatePatient();
            vm.changeProfile = true;
            swal({
                title: "Uploaded!",
                text: "Your Profile Picture Uploaded Ssuccessfully!",
                icon: "success",
                button: "OK!",
            }).then(function () {
                window.location.reload();
            });
        };
        $scope.refresh = function refresh() {
            if (vm.changeProfile)
                location.reload();
        };

        function updatePatient() {
            PatientService.UpdatePatient(vm.patient)
                .then(function () {
                    vm.cardAssigndFlag = true;
                    $location.path('/ar/patient');

                });
        }

        function logOut() {
            $rootScope.globals = {};
            AuthenticationService.ClearCredentials();
            sessionStorage.clear();
            $location.url('/login').replace();
        }

        function preview(picName, ext) {
            PatientService.getUploadFile(picName)
                .then(function (file) {
                    vm.imgPrev = file;
                    $('#imgPrev').modal('show');
                    vm.typePrev = file.split(";")[0].split("/")[1];
                    console.log(file);
                });
            // window.open('uploadedFiles/'+img, '_blank');
        }

        function add(form) {
            vm.dataLoading = true;

            if (vm.docName === '') {
                alert('please select file');
                vm.dataLoading = false;
            } else if (vm.docSize >= 10000000) {
                swal("Cant Upload!", "this file size is more than 10 MB!", "error");
                vm.dataLoading = false;

            } else {
                var f = document.getElementById('file').files[0],
                    r = new FileReader();
                r.readAsText(f);
                r.onloadend = function (e) {
                    vm.attachment.attachment = vm.attachmentValue;
                    vm.attachment.by = 'patient';

                    PatientService.uploadFiles(vm.patient.userProfile.userId, vm.docName, vm.attachment).then(function (response) {
                        if (response === 'true') {
                            vm.dataLoading = false;
                            swal({
                                title: "Uploaded!",
                                text: "Your Attachment Uploaded Successfully!",
                                icon: "success",
                            }).then(function () {
                                window.location.reload();
                                vm.attachment.doctorName = null;
                                vm.attachment.documentName = null;
                                form.documentName.$dirty = false;
                                form.doctorName.$dirty = false;
                                vm.attachment.attachment = null;
                                vm.attachmentValue = null;
                                vm.attachment = null;
                                f = null
                                $('#file').val('');

                                //
                            });
                            ;
                        } else {
                            vm.dataLoading = false;
                            swal("Cant Upload!", "this file Extention is not supported!", "error");

                        }
                    });
                    //send your binary data via $http or $resource or do anything else with it
                }
            }

        }

        function assignCard() {
            vm.card = {};
            vm.card.cardnumber = vm.patient.cardId.cardNumber;
            // vm.card.pincode = vm.patient.cardId.cardPin;
            PatientService.checkCard(vm.card)
                .then(function (response) {
                    if (response != 'false') {
                        vm.patient.cardId.isActive = "true";
                        cardService.Update(vm.patient.cardId)
                            .then(function (response) {
                                console.log(vm.patient);
                                // alert('QR Code Assignd Successfully');
                                swal({
                                    title: "Congratlation!",
                                    text: "QR Code Assignd Successfully!",
                                    icon: "success",
                                    button: "OK!",
                                });
                                $location.path('/patient');

                                // vm.patient.cardId = response;
                                // updatePatient(vm.patient);

                            });
                    } else {
                        swal(
                            'Error!',
                            'Not a valid Card Data!',
                            'error'
                        );
                    }
                });
        }


    }

})();


