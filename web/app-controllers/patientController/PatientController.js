﻿(function () {
    'use strict';

    angular
        .module('app')
        .controller('PatientController', PatientController);

    PatientController.$inject = ['PatientService', 'UserService', '$rootScope', '$location'];
    function PatientController(PatientService, UserService, $rootScope, $location) {

        $("#ar1").remove();
        $("#ar2").remove();

        var vm = this;
        var logged = false;
        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                logged = true;
        }
        if (!logged)
            $location.path('/login');
        vm.query = {}
        vm.patient = {};
        vm.user = {};
        vm.patient.government = "القاهرة";
        // vm.patient.gender = 'Male';
        vm.allPatients = [];
        vm.deletePatient = deletePatient;
        vm.register = register;
        vm.samePass = false;
        vm.emailAviable = false;
        vm.nameAviable = false;
        vm.verifyCode;
        vm.verified = false;
        vm.codeSent = false;

        vm.confirmPassword = function () {
            vm.samePass = vm.user.userPassword === vm.confirmPass ? true : false;
        };
        vm.checkMail = function () {
            UserService.checkMail(vm.user.loginName)
                .then(function (response) {
                    console.log(response);
                    if (response == 'true')
                        vm.emailAviable = true;
                    else
                        vm.emailAviable = false;
                })

        };
        vm.checkName = function () {
            PatientService.checkName(vm.patient.patientName)
                .then(function (response) {
                    console.log(response);
                    if (response == 'true')
                        vm.nameAviable = true;
                    else
                        vm.nameAviable = false;

                })

        };

        vm.sendCode = function () {
            PatientService.verifyCode(vm.patient.mobileNumber)
                .then(function (code) {
                    vm.verifyCode = code;
                    vm.codeSent = true;
                })

        }

        vm.verify = function () {

            if (vm.codeSent && vm.verifyCode != '' && vm.verificationNo != '' && vm.verificationNo == vm.verifyCode) {
                vm.verified = true;
                $('#reopen_issue').modal('hide');
                register();
                // swal({
                //     title: "Verified!",
                //     text: "Your Mobile Number is Verified Successfuly!",
                //     icon: "success",
                //     button: "OK!",
                // });
            } else {
                // $('#reopen_issue').modal('hide');
                vm.verified = false;
                // alert("not the same code");
                // swal("Try Again!", "Not The Same Code", "error");

            }

        };
        vm.addPatient = addPatient;

        initController();

        function initController() {
            // loadAllPatients();
        }

        // function loadCurrentUser() {
        //     UserService.GetByUsernames($rootScope.globals.currentUser.username)
        //         .then(function (user) {
        //             vm.user = user;
        //         });
        // }

        function loadAllPatients() {
            PatientService.GetAll()
                .then(function (patients) {
                    vm.allPatients = patients;

                });
        }

        function deletePatient(patient) {
            PatientService.Delete(patient)
                .then(function () {
                    loadAllPatients();
                });
        }

        function register() {
            vm.dataLoading = true;
            vm.user.type ='patient';

            UserService.Create(vm.user)
                .then(function (response) {
                    // if (response.success) {
                    vm.patient.patientId = response;
                    vm.patient.email = vm.user.loginName;

                    // vm.doctor.password = vm.user.userPassword;
                    addPatient();
                    // FlashService.Success('Registration successful', true);
                    // $location.path('/login');
                    // } else {
                    //     FlashService.Error(response.message);
                    //     vm.dataLoading = false;
                    // $location.path('/login');

                    // }
                });
        }

        function addPatient() {
            PatientService.Create(vm.patient)
                .then(function (response) {
                    vm.patient = null;
                    $('#reopen_issue').modal('hide');
                    swal(
                        'Congratlation!',
                        'You Regisred Successfuly!',
                        'success'
                    );
                    $location.path('/login')

                });
        }


    }

})();