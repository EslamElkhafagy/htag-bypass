﻿(function () {
    'use strict';
    const modules = [
        'ngRoute',
        'ngCookies',
        'angularFileUpload',
        'rorymadden.date-dropdowns',
        'angularCroppie',
        'ngSanitize', 'ui.select',
        'textAngular'
    ];

    angular
        .module('app', modules)
        .config(config)
        .run(run);

    config.$inject = ['$routeProvider', '$locationProvider'];

    function config($routeProvider, $locationProvider) {
        $routeProvider
            .when('/404', {
                templateUrl: 'app-resources/layout/404.html'
            })
            .when('/', {
                controller: 'LandingController',
                templateUrl: 'app-resources/landing/landing.html',
                controllerAs: 'vm'
            })
            .when('/home', {
                controller: 'LandingController',
                templateUrl: 'app-resources/landing/landing.html',
                controllerAs: 'vm'
            })
            .when('/search', {
                controller: 'LandingController',
                templateUrl: 'app-resources/landing/search.html',
                controllerAs: 'vm'
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'app-resources/login/doctor-login.html',
                controllerAs: 'vm'
            })

            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'app-resources/register/register.html',
                controllerAs: 'vm'
            })
            .when('/doctor', {
                controller: 'DoctorController',
                templateUrl: 'app-resources/doctor/home/home.html',
                controllerAs: 'vm'
            })
            .when('/doctor/profile', {
                controller: 'DoctorController',
                templateUrl: 'app-resources/doctor/home/profile.html',
                controllerAs: 'vm'
            })
            .when('/doctor/patients', {
                controller: 'DoctorController',
                templateUrl: 'app-resources/doctor/home/myPatients.html',
                controllerAs: 'vm'
            })
            .when('/doctor/setting', {
                controller: 'DoctorController',
                templateUrl: 'app-resources/doctor/home/setting.html',
                controllerAs: 'vm'
            })
            .when('/patient/doctorProfile', {
                controller: 'DoctorProfileController',
                templateUrl: 'app-resources/patient/home/DoctorProfile.html',
                controllerAs: 'vm'
            })
            .when('/patient', {
                controller: 'PatientHomeController',
                templateUrl: 'app-resources/patient/home/home.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient', {
                controller: 'PatientHomeControllerAR',
                templateUrl: 'app-resources/patient/home/home_AR.html',
                controllerAs: 'vm'
            })
            .when('/patient/docs', {
                controller: 'PatientHomeController',
                templateUrl: 'app-resources/patient/home/attchments.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient/docs', {
                controller: 'PatientHomeControllerAR',
                templateUrl: 'app-resources/patient/home/attchments_AR.html',
                controllerAs: 'vm'
            })
            .when('/patient/setting', {
                controller: 'PatientHomeController',
                templateUrl: 'app-resources/patient/home/setting.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient/setting', {
                controller: 'PatientHomeControllerAR',
                templateUrl: 'app-resources/patient/home/setting_AR.html',
                controllerAs: 'vm'
            })
            .when('/patient/qr', {
                controller: 'PatientHomeController',
                templateUrl: 'app-resources/patient/home/qrCode.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient/qr', {
                controller: 'PatientHomeControllerAR',
                templateUrl: 'app-resources/patient/home/qrCode_AR.html',
                controllerAs: 'vm'
            })
            .when('/patient/patientHistory', {
                controller: 'PatientHistoryController',
                templateUrl: 'app-resources/patient/home/PatientHistory.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient/patientHistory', {
                controller: 'PatientHistoryControllerAR',
                templateUrl: 'app-resources/patient/home/PatientHistory_AR.html',
                controllerAs: 'vm'
            })
            .when('/patient/doctorLog', {
                controller: 'DoctorLogController',
                templateUrl: 'app-resources/patient/home/doctorLog.html',
                controllerAs: 'vm'
            })
            .when('/ar/patient/doctorLog', {
                controller: 'DoctorLogControllerAR',
                templateUrl: 'app-resources/patient/home/doctorLog_AR.html',
                controllerAs: 'vm'
            })
            .when('/examinationSheet', {
                controller: 'examinationSheetController',
                templateUrl: 'app-resources/doctor/examinationSheet/examinationSheet.html',
                controllerAs: 'vm'
            })
            .when('/pharmacies/treatmentSheet', {
                controller: 'examinationSheetController',
                templateUrl: 'app-resources/doctor/examinationSheet/pharmacySheets.html',
                controllerAs: 'vm'
            })
            .when('/pharmacies', {
                controller: 'PharmaciesController',
                templateUrl: 'app-resources/pharmacies/home/home.html',
                controllerAs: 'vm'
            })
            .when('/laps', {
                controller: 'LapsController',
                templateUrl: 'app-resources/laps/home/home.html',
                controllerAs: 'vm'
            })
            .when('/laps/currentTest', {
                controller: 'LapsController',
                templateUrl: 'app-resources/laps/home/currentTest.html',
                controllerAs: 'vm'
            })
            .when('/laps/analysisSheet', {
                controller: 'examinationSheetController',
                templateUrl: 'app-resources/doctor/examinationSheet/analysisSheet.html',
                controllerAs: 'vm'
            })
            .when('/laps/analysisSheet/:addAnalysis', {
                controller: 'examinationSheetController',
                templateUrl: 'app-resources/doctor/examinationSheet/analysisSheet.html',
                controllerAs: 'vm'
            })
            .when('/radiation', {
                controller: 'RadiationsController',
                templateUrl: 'app-resources/radiations/home/home.html',
                controllerAs: 'vm'
            })
            .when('/radiation/radiationSheet', {
                controller: 'examinationSheetController',
                templateUrl: 'app-resources/doctor/examinationSheet/radiationSheet.html',
                controllerAs: 'vm'
            })
            .when('/radiation/currentTest', {
                controller: 'RadiationsController',
                templateUrl: 'app-resources/radiations/home/currentTest.html',
                controllerAs: 'vm'
            })
            .when('/radiation/setting', {
                controller: 'RadiationsController',
                templateUrl: 'app-resources/radiations/home/setting.html',
                controllerAs: 'vm'
            })
            .when('/laps/setting', {
                controller: 'LapsController',
                templateUrl: 'app-resources/laps/home/setting.html',
                controllerAs: 'vm'
            })
            .when('/pharmacies/setting', {
                controller: 'PharmaciesController',
                templateUrl: 'app-resources/pharmacies/home/setting.html',
                controllerAs: 'vm'
            })
            .when('/pharmacies/patientLog', {
                controller: 'PharmaciesController',
                templateUrl: 'app-resources/pharmacies/home/patientLog.html',
                controllerAs: 'vm'
            })
            .when('/radiation/patientLog', {
                controller: 'RadiationsController',
                templateUrl: 'app-resources/radiations/home/patientLog.html',
                controllerAs: 'vm'
            })
            .when('/laps/patientLog', {
                controller: 'LapsController',
                templateUrl: 'app-resources/laps/home/patientLog.html',
                controllerAs: 'vm'
            })
            .when('/qr', {
                controller: 'PatientController',
                templateUrl: 'bower_components/test.html',
                controllerAs: 'vm'
            })
            .when('/qr2', {
                controller: 'DemoCtrl',
                templateUrl: 'bc-qr-reader/demo/test.html',
                controllerAs: 'asc'
            })
            .when('/forgetPassword', {
                controller: 'RegisterController',
                templateUrl: 'app-resources/login/forgetPassword.html',
                controllerAs: 'asc'
            })
            .otherwise({redirectTo: '/404'});
    }

    run.$inject = ['$rootScope', '$location', '$cookies', '$http', '$route', 'AuthenticationService', 'ClinicService'
        , "PatientService", 'UserService'];

    function run($rootScope, $location, $cookies, $http, $route, AuthenticationService, ClinicService,
                 PatientService, UserService) {
        $rootScope.showLoader = false
        $rootScope.search = false;
        // keep user logged in after page refresh
        $rootScope.globals = $cookies.getObject('globals') || {};
        var url = 'http://http://www.htag.health/#!';
        // var url = 'http://127.0.0.1:8280/byba_ss_war_exploded/#!';
        var type = '';
        var loggedIn = false;

        if ($rootScope.globals) {
            if ($rootScope.globals.currentUser)
                loggedIn = true;
        }
        if (loggedIn) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
            type = $rootScope.globals.currentUser.type;
        }
        // else {
        //     $location.path('/login');
        // }
        $rootScope.$on('$locationChangeStart', function (event, next, current) {
            $("#ar1").remove();
            $("#ar2").remove();

            loggedIn = false;
            if ($rootScope.globals) {
                if ($rootScope.globals.currentUser)
                    loggedIn = true;
            }
            var pageType = "";
            // pageType = getPageType($location.path());
            // var x = false;
            // if (pageType === 'generalPages' && $location.path() !== '/login')
            //     x = true;
            // console.log(pageType);
            // var absUrl = $location.absUrl();
            var t = current.split("!");
            var h = $location.path();
            var f = $.inArray($location.path(), generalPages);
            var loc = url + '/login'
            if (next == url + '/login' && loggedIn && f < 0)
                $location.path(t[1]);
            // var route_object = ($route.routes[$location.path()]).route_object; //This is how you get it
            // console.log(t);
            // console.log("event", event);
            // console.log("next", next);
            // console.log("current", current);
            // console.log("$location.path()", $location.path())

        });
        // $rootScope.$on('$locationChangeStart', function (event, next, current) {
        //     // redirect to login page if not logged in and trying to access a restricted page
        //
        //     var loggedIn = $rootScope.globals.currentUser;
        //     var pageType = getPageType($location.path());
        //
        //     if (!loggedIn && pageType !== 'generalPages') {
        //         $location.path('/login');
        //     } else if ($location.path() === '/patientLog' && loggedIn && (type == 'patient')) {
        //         $location.path('/login');
        //     } else if (pageType === 'patientPages' && loggedIn && type !== 'patient') {
        //         $location.path('/login');
        //     } else if (pageType === 'doctorPages' && loggedIn && type !== 'doctor') {
        //         $location.path('/login');
        //     } else if (pageType === 'pharmaciesPages' && loggedIn && type !== 'pharmacies') {
        //         $location.path('/login');
        //     } else if (pageType === 'lapsPages' && loggedIn && type !== 'laps') {
        //         $location.path('/login');
        //     } else if (pageType === 'radiationsPages' && loggedIn && type !== 'radiations') {
        //         $location.path('/login');
        //     }
        // });

        $rootScope.logOut = function () {
            localStorage.clear();
            ClinicService.flag = 0;
            ClinicService.ClinicId = 0;
            $rootScope.globals = {};
            sessionStorage.clear();
            PatientService.patient = undefined;
            PatientService.allAttchFiles = undefined;
            PatientService.patientId = undefined;
            AuthenticationService.ClearCredentials();
        }
        $rootScope.GetPatientByCardNumber = function (CardNumber) {
            $rootScope.search = false;
            PatientService.GetPatientByCardNumber(CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        localStorage.setItem("PatientId", patient.patientId);
                        $rootScope.search = false;

                        if ($location.url() === '/examinationSheet') {

                            location.reload();
                        } else {
                            $location.url('/examinationSheet').replace();
                        }

                    } else {
                        $rootScope.search = true;
                    }

                });
        };
        $rootScope.GetPatientByCardNumberLab = function (CardNumber) {
            $rootScope.search = false;
            PatientService.GetPatientByCardNumber(CardNumber)
                .then(function (patient) {
                    if (patient !== null) {
                        localStorage.setItem("PatientId", patient.patientId);
                        $rootScope.search = false;

                        if ($location.url() === '/laps/analysisSheet') {

                            location.reload();
                        } else {
                            $location.url('/laps/analysisSheet').replace();
                        }

                    } else {
                        $rootScope.search = true;
                    }

                });
        }
        /// this part is for add patient from header any where
        function initPatientData(form) {
            $rootScope.patient = {};
            $rootScope.patient.userProfile = {};
            $rootScope.patient.mobileNumber = '';
            $rootScope.patient.patientName = '';
            $rootScope.patient.cardId = {};
            $rootScope.patient.cardId.cardNumber = '';
            $rootScope.patient.smoker = '';
            $rootScope.patient.hypertension = '';
            $rootScope.patient.diabetes = '';
            $rootScope.showInvalidMobileNo = false;
            $rootScope.mobileAviable = true;

            if (form) {
                form.$setPristine(true);
            }
        }

        initPatientData();
        $rootScope.checkMobile = function (mobileNo) {
            if (mobileNo) {
                if (!/^(010|011|012|015|013){1}([0-9]{8})$/.test(mobileNo)) {
                    $rootScope.showInvalidMobileNo = true;
                    $rootScope.mobileAviable = true;
                } else {
                    $rootScope.showInvalidMobileNo = false;
                    UserService.checkMobile(mobileNo)
                        .then(function (response) {
                            if (response == 'true')
                                $rootScope.mobileAviable = true;
                            else
                                $rootScope.mobileAviable = false;

                        })
                }
            } else {
                $rootScope.mobileAviable = true;
                $rootScope.showInvalidMobileNo = false;
            }
        };
        $rootScope.addPatient = function (form) {
            $rootScope.patient.cardId.cardPin = '';
            $rootScope.patient.userProfile = {};
            $rootScope.patient.userProfile.loginName = $rootScope.patient.mobileNumber;
            PatientService.addPatient($rootScope.patient)
                .then(function (response) {
                    if (response.status == '0') {
                        swal(
                            'Error!',
                            'Not a valid Card Data!',
                            'error'
                        )
                    }
                    else if (response.status == '2') {
                        swal(
                            'Error!',
                            'Error in Adding new patient!',
                            'error'
                        );
                    } else if (response.status == '1') {
                        $rootScope.patient = null;
                        $('#myModal3').modal('hide');

                        swal(
                            'Congratulation!',
                            'patient Created Successfully!',
                            'success'
                        ).then(function () {
                            localStorage.setItem("PatientId", response.patient.patientId);
                            if ($rootScope.globals.currentUser.type === 'laps') {
                                PatientService.openAddNewAnalysisOnLab = true;
                                $location.path('/laps/analysisSheet');
                                $route.reload();
                                // window.location.href = "#!/laps/analysisSheet";
                            } else if ($rootScope.globals.currentUser.type === 'doctor') {
                                window.location.href = "#!/examinationSheet";
                            } else {
                            }

                            // console.log($rootScope.globals.currentUser.type);

                            // window.location.href = "/byba_ss_war_exploded/#!/examinationSheet";
                            // location.reload();
                        });
                    }
                });
        }
    }

    var doctorPages = ['/doctor', '/doctor/patientLog', '/doctor/setting'];
    var patientPages = ['/patient', '/patient/qr', '/patient/doctorLog', '/patient/setting', '/doctorProfile'];
    var pharmaciesPages = ['/pharmacies', '/pharmacies/setting', '/laps/patientLog'];
    var lapsPages = ['/laps', '/laps/setting', '/laps/patientLog'];
    var radiationsPages = ['/radiations', '/radiations/setting', '/laps/patientLog'];
    var generalPages = ['/register', '/home', '/search', "/login"];

    function getPageType(pageName) {

        if ($.inArray(pageName, generalPages) >= 0) {
            return "generalPages";
        } else if ($.inArray(pageName, patientPages) >= 0) {
            return "patientPages";
        } else if ($.inArray(pageName, doctorPages) >= 0) {
            return "doctorPages";
        } else if ($.inArray(pageName, pharmaciesPages) >= 0) {
            return "pharmaciesPages";
        } else if ($.inArray(pageName, lapsPages) >= 0) {
            return "lapsPages";
        } else if ($.inArray(pageName, radiationsPages) >= 0) {
            return "radiationsPages";
        }

    }

})();
