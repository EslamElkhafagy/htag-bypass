(function () {
  'use strict';

  angular
      .module('app')
      .controller('DemoCtrl', DemoCtrl);

  function DemoCtrl($scope) {
    var asc = this;
    asc.start = start;
    asc.processURLfromQR = processURLfromQR;
    initController();

    function initController() {

    }


    function start() {
      asc.cameraRequested = true;
    }

   function processURLfromQR (url) {
     asc.url = url;
     asc.cameraRequested = false;
    }


  }

})();